#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <getopt.h>

#include <redex/redex.h>
#include <redex/assembler.h>

const char help[] = 
    "Redex (" __DATE__ " " __TIME__ ")\n"
    "Usage: redex-secd [option] [file] [file]\n"
    "Example: redex-secd compiler.secd compiler.lisp";

int is_verbose = 0;
int print = 0;
int assemble = 0;

int main(int argc, char *argv[])
{
    FILE *fp[2];
    int fpi = 0;
    struct redex_object *fn, *args, *result;
    int ch, arg;

    while ((ch = getopt(argc, argv, "vhps")) != -1) {
        switch(ch) {
            case 'h':
                puts(help);
                return 0;
            case 'v':
                is_verbose++;
                break;
            case 'p':
                print = 1;
                break;
            case 's':
                assemble = 1;
                break;
            case '?':
                return -1;
        }
    }

    fp[0] = stdin;
    fp[1] = stdin;

    for(fpi = 0, arg = optind; (arg < argc) && (fpi < 2); arg++, fpi++) {
        fp[fpi] = fopen(argv[arg], "ra");
        if (fp[fpi] == NULL || ferror(fp[fpi]) != 0) {
            printf("Could not load '%s': %s\n", argv[arg], strerror(errno));
            exit(-1);
        }
    }

    struct redex_secd secd;
    struct redex_ctx ctx;

    memset(&ctx, 0, sizeof(ctx));
    memset(&secd, 0, sizeof(secd));

    ctx.secd = &secd;

    redex_init(&ctx);

    fn = get_exp(&ctx, fp[0]);
    args = get_exp_list(&ctx, fp[1]);
    result = redex_execute(&ctx, fn, args);

    if (print) {
        exp_print(&ctx, result);
        printf("\n");
    }

    if (assemble) {
        redex_assemble(&ctx, result);
        printf("\n");
    }

    fclose(fp[0]);
    fclose(fp[1]);

    if (is_verbose) {
        redex_gc_stats(&ctx);
    }

    redex_gc_exit(&ctx);
    intern_free();

    return 0;
}
