#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <assert.h>

#include <redex/secd.h>
#include <redex/gc.h>

static struct redex_object *locate(unsigned i, unsigned j, struct redex_object *env)
{
    struct redex_object *obj = env;

    while (i--)
        obj = redex_cdr(obj);

    obj = redex_car(obj);

    while (j--)
        obj = redex_cdr(obj);

    obj = redex_car(obj);

    return obj;
}

static struct redex_object *lcons(struct redex_ctx *ctx, int nr, ...)
{
    struct redex_object *objs[nr];

    va_list args;
    va_start(args, nr);

    for (int i = 0; i < nr; ++i) {
        objs[i] = va_arg(args, struct redex_object *);
    }

    struct redex_object *obj = objs[nr-1];

    for (int i = nr - 2; i >= 0; --i) {
        obj = redex_cons(ctx, objs[i], obj);
    }

    va_end(args);

    return obj;
}


static void secd_ldc(struct redex_ctx *ctx)
{
    /*
     * ldc: S, E, (LDC x C), D -> (x S), E, C, D
     */

    struct redex_secd *secd = ctx->secd;

    struct redex_object *S = secd->stack;
    struct redex_object *x = redex_car(redex_cdr(secd->control));
    struct redex_object *C = redex_cdr(redex_cdr(secd->control));

    secd->stack   = redex_cons(ctx, x, S);
    secd->control = C;
}

static void secd_ld(struct redex_ctx *ctx)
{
    /*
     * ld: S, E, (LD (i j) C), D -> (locate((i, j), E) S), E, C, D
     */

    struct redex_secd *secd = ctx->secd;

    struct redex_object *pair = redex_car(redex_cdr(secd->control));
    unsigned i = number_value(redex_car(pair));
    unsigned j = number_value(redex_cdr(pair));

    struct redex_object *obj = locate(i, j, secd->environ);
    ctx->work = obj;

    secd->stack   = redex_cons(ctx, obj, secd->stack);
    secd->control = redex_cdr(redex_cdr(secd->control));
}

static void secd_car(struct redex_ctx *ctx)
{
    /*
     * car: ((h t) S), E, (CAR C), D -> (h S), E, C, D
     */

    struct redex_secd *secd = ctx->secd;

    struct redex_object *obj = redex_car(secd->stack);
    secd->stack   = redex_cons(ctx, redex_car(obj), redex_cdr(secd->stack));
    secd->control = redex_cdr(secd->control);
}

static void secd_cdr(struct redex_ctx *ctx)
{
    /*
     * cdr: ((h t) S), E, (CDR C), D -> (t S), E, C, D
     */

    struct redex_secd *secd = ctx->secd;

    struct redex_object *obj = redex_car(secd->stack);
    secd->stack   = redex_cons(ctx, redex_cdr(obj), redex_cdr(secd->stack));
    secd->control = redex_cdr(secd->control);
}

static void secd_atom(struct redex_ctx *ctx)
{
    /*
     * atom: (e S), E, (ATOM C), D
     *      -> (true S), E, C, D     (if e is an atom)
     *      -> (false S), E, C, D    (otherwise)
     */

    struct redex_secd *secd = ctx->secd;

    struct redex_object *obj = redex_car(secd->stack);

    if (REDEX_IS_ATOM(obj)) {
        secd->stack = redex_cons(ctx, ctx->_true, redex_cdr(secd->stack));
    } else {
        secd->stack = redex_cons(ctx, ctx->_false, redex_cdr(secd->stack));
    }

    secd->control = redex_cdr(secd->control);
}

static void secd_cons(struct redex_ctx *ctx)
{
    /*
     * cons: (a b S), E, (CONS C), D -> ((a b) S), E, C, D
     */

    struct redex_secd *secd = ctx->secd;

    struct redex_object *a = redex_car(secd->stack);
    struct redex_object *b = redex_car(redex_cdr(secd->stack));
    struct redex_object *obj = redex_cons(ctx, a,b);

    secd->stack   = redex_cons(ctx, obj, redex_cdr(redex_cdr(secd->stack)));
    secd->control = redex_cdr(secd->control);
}

static void secd_sub(struct redex_ctx *ctx)
{
    /*
     * sub: (a b S), E, (SUB C), D -> (r S), E, C, D
     *  where r = b - a
     */

    struct redex_secd *secd = ctx->secd;

    struct redex_object *a = redex_car(secd->stack);
    struct redex_object *b = redex_car(redex_cdr(secd->stack));

    int r = number_value(b) - number_value(a);

    secd->stack   = redex_cons(ctx, redex_number(ctx, r), redex_cdr(redex_cdr(secd->stack)));
    secd->control = redex_cdr(secd->control);
}

static void secd_add(struct redex_ctx *ctx)
{
    /*
     * add: (a b S), E, (ADD C), D -> (r S), E, C, D
     *  where r = b + a
     */

    struct redex_secd *secd = ctx->secd;


    struct redex_object *a = redex_car(secd->stack);
    struct redex_object *b = redex_car(redex_cdr(secd->stack));

    int r = number_value(b) + number_value(a);

    secd->stack   = redex_cons(ctx, redex_number(ctx, r), redex_cdr(redex_cdr(secd->stack)));
    secd->control = redex_cdr(secd->control);
}

static void secd_mul(struct redex_ctx *ctx)
{
    /*
     * mul: (a b S), E, (MUL C), D -> (r S), E, C, D
     *  where r = b * a
     */

    struct redex_secd *secd = ctx->secd;

    struct redex_object *a = redex_car(secd->stack);
    struct redex_object *b = redex_car(redex_cdr(secd->stack));

    int r = number_value(b) * number_value(a);

    secd->stack   = redex_cons(ctx, redex_number(ctx, r), redex_cdr(redex_cdr(secd->stack)));
    secd->control = redex_cdr(secd->control);
}

static void secd_div(struct redex_ctx *ctx)
{
    /*
     * div: (a b S), E, (DIV C), D -> (r S), E, C, D
     *  where r = b / a
     */

    struct redex_secd *secd = ctx->secd;

    struct redex_object *a = redex_car(secd->stack);
    struct redex_object *b = redex_car(redex_cdr(secd->stack));

    int r = number_value(b) / number_value(a);

    secd->stack   = redex_cons(ctx, redex_number(ctx, r), redex_cdr(redex_cdr(secd->stack)));
    secd->control = redex_cdr(secd->control);
}

static void secd_rem(struct redex_ctx *ctx)
{
    /*
     * rem: (a b S), E, (REM C), D -> (r S), E, C, D
     *  where r = b % a
     */

    struct redex_secd *secd = ctx->secd;

    struct redex_object *a = redex_car(secd->stack);
    struct redex_object *b = redex_car(redex_cdr(secd->stack));

    int r = number_value(b) % number_value(a);

    secd->stack   = redex_cons(ctx, redex_number(ctx, r), redex_cdr(redex_cdr(secd->stack)));
    secd->control = redex_cdr(secd->control);
}

static void secd_leq(struct redex_ctx *ctx)
{
    /*
     * leq: (a b S), E, (LEQ C), D -> (r S), E, C, D
     *  where r = b <= a
     */

    struct redex_secd *secd = ctx->secd;

    struct redex_object *a = redex_car(secd->stack);
    struct redex_object *b = redex_car(redex_cdr(secd->stack));

    int r = number_value(b) <= number_value(a);

    if (r) {
        secd->stack = redex_cons(ctx, ctx->_true, redex_cdr(redex_cdr(secd->stack)));
    } else {
        secd->stack = redex_cons(ctx, ctx->_false, redex_cdr(redex_cdr(secd->stack)));
    }

    secd->control = redex_cdr(secd->control);
}

static void secd_eq(struct redex_ctx *ctx)
{
    /*
     * eq: (a b S), E, (EQ C), D -> (r S), E, C, D
     *  where r = b == a
     */

    struct redex_secd *secd = ctx->secd;

    struct redex_object *a = redex_car(secd->stack);
    struct redex_object *b = redex_car(redex_cdr(secd->stack));

    int r = 0;

    if (REDEX_IS_SYMBOL(a) && REDEX_IS_SYMBOL(b)) {
        r = !strcmp(string_value(a), string_value(b));
    } else if (REDEX_IS_NUMBER(a) && REDEX_IS_NUMBER(b)) {
        r = number_value(a) == number_value(b);
    }

    if (r) {
        secd->stack = redex_cons(ctx, ctx->_true, redex_cdr(redex_cdr(secd->stack)));
    } else {
        secd->stack = redex_cons(ctx, ctx->_false, redex_cdr(redex_cdr(secd->stack)));
    }

    secd->control = redex_cdr(secd->control);
}

static void secd_ldf(struct redex_ctx *ctx)
{
    /*
     * ldf: S, E, (LDF f C), D -> ((f E) S), E, C, D
     */

    struct redex_secd *secd = ctx->secd;

    struct redex_object *f = redex_car(redex_cdr(secd->control));
    struct redex_object *closure = redex_cons(ctx, f, secd->environ);

    secd->stack = redex_cons(ctx, closure, secd->stack);
    secd->control = redex_cdr(redex_cdr(secd->control));
}

static void secd_rtn(struct redex_ctx *ctx)
{
    /*
     * rtn: (r S), E, (RTN C), (s e c D) -> (r s), e, c, D
     */

    struct redex_secd *secd = ctx->secd;

    struct redex_object *r = redex_car(secd->stack);
    struct redex_object *s = redex_car(secd->dump);

    secd->stack   = redex_cons(ctx, r, s);
    secd->environ = redex_car(redex_cdr(secd->dump));
    secd->control = redex_car(redex_cdr(redex_cdr(secd->dump)));
    secd->dump    = redex_cdr(redex_cdr(redex_cdr(secd->dump)));
}

static void secd_dum(struct redex_ctx *ctx)
{
    /*
     * dum: S, E, (DUM C), D -> S, (nil E), C, D
     */

    struct redex_secd *secd = ctx->secd;

    secd->environ = redex_cons(ctx, ctx->_nil, secd->environ);
    secd->control = redex_cdr(secd->control);
}

static void secd_rplaca(struct redex_ctx *ctx)
{
    /*
     * rplaca: (c o S), E, (RPLACA C), D -> (c' S), E, C, D
     *  where car(c') = o;
     */

    struct redex_secd *secd = ctx->secd;

    struct redex_object *_cons = redex_car(secd->stack);
    struct redex_object *obj   = redex_car(redex_cdr(secd->stack));

    redex_rplaca(_cons, obj);

    secd->stack   = redex_cons(ctx, _cons, redex_cdr(redex_cdr(secd->stack)));
    secd->control = redex_cdr(secd->control);
}

static void secd_rap(struct redex_ctx *ctx)
{
    /*
     * rap: ((f e) v S), (w E), (RAP C), D -> nil, e, C, (S E C D)
     *  where car e = v
     */

    struct redex_secd *secd = ctx->secd;

    struct redex_object *S = redex_cdr(redex_cdr(secd->stack));
    struct redex_object *E = redex_cdr(secd->environ);
    struct redex_object *C = redex_cdr(secd->control);
    struct redex_object *D = secd->dump;

    struct redex_object *f = redex_car(redex_car(secd->stack));
    struct redex_object *e = redex_cdr(redex_car(secd->stack));
    struct redex_object *v = redex_car(redex_cdr(secd->stack));

    secd->stack = ctx->_nil;
    secd->environ = e;
    secd->control = f;
    secd->dump = lcons(ctx, 4, S, E, C, D);

    redex_rplaca(e, v);
}

static void secd_sel(struct redex_ctx *ctx)
{
    /*
     * join: S, E, _, (C D) -> S, E, C, D
     */

    struct redex_secd *secd = ctx->secd;

    secd->dump = redex_cons(ctx, redex_cdr(redex_cdr(redex_cdr(secd->control))),secd->dump);
    if (strcmp(string_value(redex_car(secd->stack)), string_value(ctx->_true)) == 0) {
        secd->control = redex_car(redex_cdr(secd->control));
    } else {
        secd->control = redex_car(redex_cdr(redex_cdr(secd->control)));
    }
    secd->stack   = redex_cdr(secd->stack);
}

static void secd_join(struct redex_ctx *ctx)
{
    /*
     * join: S, E, _, (C D) -> S, E, C, D
     */

    struct redex_secd *secd = ctx->secd;

    secd->control = redex_car(secd->dump);
    secd->dump = redex_cdr(secd->dump);
}

static void secd_ap(struct redex_ctx *ctx)
{
    /*
     * ap: ((f e) v S), E, (AP C), D -> nil, (v e), f, (S E C D)
     */

    struct redex_secd *secd = ctx->secd;

    struct redex_object *s = redex_cdr(redex_cdr(secd->stack));
    struct redex_object *e = secd->environ;
    struct redex_object *c = redex_cdr(secd->control);
    struct redex_object *d = secd->dump;

    struct redex_object *f = redex_car(redex_car(secd->stack));
    struct redex_object *v = redex_car(redex_cdr(secd->stack));
    struct redex_object *_e = redex_cdr(redex_car(secd->stack));

    secd->dump    = lcons(ctx, 4, s, e, c, d);
    secd->environ = redex_cons(ctx, v, _e);
    secd->control = f;
    secd->stack   = ctx->_nil;
}

static void secd_stop(struct redex_ctx *ctx)
{
    struct redex_secd *secd = ctx->secd;
    secd->stopped = 1;
}

static void secd_call(struct redex_ctx *ctx)
{
    struct redex_secd *secd = ctx->secd;

    struct redex_object *obj = redex_car(secd->stack);

    secd->stack = redex_cdr(secd->stack);
    secd->control = redex_cdr(secd->control);

    const char *s = string_value(obj);

    redex_api_call(ctx, s);
}

struct {
    const char *mnemonic;
    void (*op)(struct redex_ctx *);
} inst_set[] = {
    {"LD",     secd_ld},
    {"LDC",    secd_ldc},
    {"LDF",    secd_ldf},
    {"AP",     secd_ap},
    {"RTN",    secd_rtn},
    {"DUM",    secd_dum},
    {"RAP",    secd_rap},
    {"SEL",    secd_sel},
    {"JOIN",   secd_join},
    {"CAR",    secd_car},
    {"CDR",    secd_cdr},
    {"ATOM",   secd_atom},
    {"CONS",   secd_cons},
    {"EQ",     secd_eq},
    {"ADD",    secd_add},
    {"SUB",    secd_sub},
    {"MUL",    secd_mul},
    {"DIV",    secd_div},
    {"REM",    secd_rem},
    {"LEQ",    secd_leq},
    {"RPLACA", secd_rplaca},
    {"CALL",   secd_call},
    {"STOP",   secd_stop},
};

#define INST_CNT (sizeof(inst_set)/sizeof(inst_set[0]))

void (*get_op(const char *s))(struct redex_ctx *)
{
    for (int i = 0; i < INST_CNT; ++i) {
        if (!strcmp(s, inst_set[i].mnemonic))
            return inst_set[i].op;
    }

    return NULL;
}

struct redex_object *redex_execute(struct redex_ctx *ctx, struct redex_object *fn, struct redex_object *args)
{
    struct redex_secd *secd = ctx->secd;

    secd->stack   = redex_cons(ctx, args, ctx->_nil);
    secd->environ = ctx->_nil;
    secd->control = fn;
    secd->dump    = ctx->_nil;

    secd->stopped = 0;

    while (!secd->stopped) {
        struct redex_object *c = redex_car(secd->control);
        const char *mnemonic = string_value(c);
        void (*op)(struct redex_ctx *) = get_op(mnemonic);

        if (op) {
            op(ctx);
        } else {
            fprintf(stderr, "unsupported instruction \"%s\"\n", mnemonic);
            exit(-1);
        }
    }

    return secd->stack;
}
