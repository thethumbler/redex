export

PDIR != pwd

CFLAGS += -I$(PDIR)/include
LDFLAGS += -L$(PDIR)/libredex

all: compiler/redexc

.PHONY: compiler/redexc
compiler/redexc: src/redex-secd
	$(MAKE) -C compiler/

.PHONY: src/redex-secd
src/redex-secd: libredex/libredex.so
	$(MAKE) -C src/

.PHONY: libredex/libredex.so
libredex/libredex.so:
	$(MAKE) -C libredex/

.PHONY: clean
clean:
	$(MAKE) -C libredex/ clean
	$(MAKE) -C src/ clean
	$(MAKE) -C compiler/ clean
