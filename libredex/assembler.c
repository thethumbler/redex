#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#include <redex/redex.h>
#include <redex/object.h>
#include <redex/assembler.h>

#define BLOCK_MAX 8192
#define INDENT  "    "

static int closure_id = 0;

struct closure {
    int id;
    char *compiled;
    char *ptr;

    struct redex_object *obj;

    struct closure *next;
};

struct closure *cur_closure = NULL;

void asm_loadk(struct redex_ctx *ctx, struct closure *closure, struct redex_object *obj)
{
    if (REDEX_IS_NUMBER(obj)) {
        closure->ptr += sprintf(closure->ptr, INDENT "inst_ldc_int(%d);\n", number_value(obj));
    } else if (REDEX_IS_SYMBOL(obj)) {
        closure->ptr += sprintf(closure->ptr, INDENT "inst_ldc_str(\"%s\");\n", string_value(obj));
    } else if (REDEX_IS_CONS(obj)) {
        closure->ptr += sprintf(closure->ptr, INDENT "inst_ldc_cons((struct redex_object *[]){");

        while (!REDEX_IS_NULL(ctx, obj)) {
            struct redex_object *o = redex_car(obj);
            if (REDEX_IS_SYMBOL(o))
                closure->ptr += sprintf(closure->ptr, "redex_symbol(rctx, \"%s\"), ", string_value(o));
            obj = redex_cdr(obj);
        }

        closure->ptr += sprintf(closure->ptr, "NULL});\n");
    } else {
        fprintf(stderr, "Unknown constant type %s\n", type_str(obj->type));
        exit(-1);
    }
}

void asm_load(struct redex_ctx *ctx, struct closure *closure, struct redex_object *idx)
{
    int i = number_value(redex_car(idx));
    int j = number_value(redex_cdr(idx));
    closure->ptr += sprintf(closure->ptr, INDENT "inst_ld(%d, %d);\n", i, j);
}

struct closure *asm_closure(struct redex_ctx *ctx, struct redex_object *obj)
{
    struct closure *block = malloc(sizeof(struct closure)); 

    block->id = closure_id++;
    block->compiled = calloc(1, BLOCK_MAX);
    block->ptr = block->compiled;
    block->obj = obj;
    block->next = cur_closure;
    cur_closure = block;

    struct redex_object *instp = obj;

    while (!REDEX_IS_NULL(ctx, instp)) {
        struct redex_object *inst = redex_car(instp);

        int advance = 1;

        const char *mnemonic = string_value(inst);

        if (!strcmp(mnemonic, "LDF")) {
            struct redex_object *f = redex_car(redex_cdr(instp));
            struct closure *closure = asm_closure(ctx, f);
            block->ptr += sprintf(block->ptr, INDENT "inst_ldf(closure_%d);\n", closure->id);

            redex_rplaca(redex_cdr(instp), redex_closure(ctx, (void *) (uintptr_t) closure->id));

            advance = 2;
        } else if (!strcmp(mnemonic, "LDC")) {
            struct redex_object *symbol = redex_car(redex_cdr(instp));
            asm_loadk(ctx, block, symbol);
            advance = 2;
        } else if (!strcmp(mnemonic, "LD")) {
            struct redex_object *idx = redex_car(redex_cdr(instp));
            asm_load(ctx, block, idx);
            advance = 2;
        } else if (!strcmp(mnemonic, "SEL")) {
            struct redex_object *ct = redex_car(redex_cdr(instp));
            struct redex_object *cf = redex_car(redex_cdr(redex_cdr(instp)));

            struct closure *t_closure = asm_closure(ctx, ct);
            struct closure *f_closure = asm_closure(ctx, cf);

            block->ptr += sprintf(block->ptr, INDENT "inst_sel(closure_%d, closure_%d);\n", t_closure->id, f_closure->id);

            redex_rplaca(redex_cdr(instp), redex_closure(ctx, (void *) (uintptr_t) t_closure->id));
            redex_rplaca(redex_cdr(redex_cdr(instp)), redex_closure(ctx, (void *) (uintptr_t) f_closure->id));

            advance = 3;
        } else if (!strcmp(mnemonic, "CONS")) {
            block->ptr += sprintf(block->ptr, INDENT "inst_cons();\n");
        } else if (!strcmp(mnemonic, "CALL")) {
            block->ptr += sprintf(block->ptr, INDENT "inst_call();\n");
        } else if (!strcmp(mnemonic, "RTN")) {
            block->ptr += sprintf(block->ptr, INDENT "inst_rtn();\n");
        } else if (!strcmp(mnemonic, "AP")) {
            block->ptr += sprintf(block->ptr, INDENT "inst_ap();\n");
        } else if (!strcmp(mnemonic, "STOP")) {
            block->ptr += sprintf(block->ptr, INDENT "inst_stop();\n");
        } else if (!strcmp(mnemonic, "DUM")) {
            block->ptr += sprintf(block->ptr, INDENT "inst_dum();\n");
        } else if (!strcmp(mnemonic, "EQ")) {
            block->ptr += sprintf(block->ptr, INDENT "inst_eq();\n");
        } else if (!strcmp(mnemonic, "JOIN")) {
            block->ptr += sprintf(block->ptr, INDENT "inst_join();\n");
        } else if (!strcmp(mnemonic, "ADD")) {
            block->ptr += sprintf(block->ptr, INDENT "inst_add();\n");
        } else if (!strcmp(mnemonic, "SUB")) {
            block->ptr += sprintf(block->ptr, INDENT "inst_sub();\n");
        } else if (!strcmp(mnemonic, "MUL")) {
            block->ptr += sprintf(block->ptr, INDENT "inst_mul();\n");
        } else if (!strcmp(mnemonic, "DIV")) {
            block->ptr += sprintf(block->ptr, INDENT "inst_div();\n");
        } else if (!strcmp(mnemonic, "REM")) {
            block->ptr += sprintf(block->ptr, INDENT "inst_rem();\n");
        } else if (!strcmp(mnemonic, "CDR")) {
            block->ptr += sprintf(block->ptr, INDENT "inst_cdr();\n");
        } else if (!strcmp(mnemonic, "CAR")) {
            block->ptr += sprintf(block->ptr, INDENT "inst_car();\n");
        } else if (!strcmp(mnemonic, "RAP")) {
            block->ptr += sprintf(block->ptr, INDENT "inst_rap();\n");
        } else if (!strcmp(mnemonic, "ATOM")) {
            block->ptr += sprintf(block->ptr, INDENT "inst_atom();\n");
        } else {
            exp_print(ctx, inst);
            printf("\n");
            exit(-1);
        }

        while (REDEX_IS_CONS(instp) && advance--)
            instp = redex_cdr(instp);

        if (!REDEX_IS_CONS(instp))
            break;
    }

    return block;
}

void redex_assemble(struct redex_ctx *ctx, struct redex_object *obj)
{
    if (!obj || !REDEX_IS_CONS(obj))
        return;

    asm_closure(ctx, redex_car(obj));

    struct closure *closure = cur_closure;

    printf("#include <redex/runtime.h>\n\n");

    while (closure) {
        printf("// closure %d\n// ", closure->id);
        exp_print(ctx, closure->obj);
        printf("\n");
        fflush(stdout);

        printf("%svoid closure_%d()\n{\n", closure->id? "static inline " : "", closure->id);
        printf(closure->compiled);
        printf("}\n\n");
        free(closure->compiled);
        closure = closure->next;
    }
}
