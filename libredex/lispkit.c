#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>

#include <redex/gc.h>

struct redex_object *redex_object_alloc(struct redex_ctx *ctx)
{
    return redex_gc_alloc(ctx);
}

struct redex_object *redex_cons(struct redex_ctx *ctx, struct redex_object *_car, struct redex_object *_cdr)
{
    assert(ctx);
    assert(_car);
    assert(_cdr);

    struct redex_object *obj = redex_object_alloc(ctx);
    obj->type = REDEX_CONS;
    obj->cons.car = _car;
    obj->cons.cdr = _cdr;
    return obj;
}

struct redex_object *redex_car(struct redex_object *obj)
{
    assert(obj);

    if (obj->type != REDEX_CONS) {
        printf("CAR error: not possible on type \"%s\"\n", 
                type_str(obj->type));
        exit(-1);
    }

    return obj->cons.car;
}

struct redex_object *redex_cdr(struct redex_object *obj)
{
    if (obj->type != REDEX_CONS) {
        printf("CDR error: not possible on type \"%s\"\n",
                type_str(obj->type));
        exit(-1);
    }

    return obj->cons.cdr;
}

struct redex_object *redex_rplaca(struct redex_object *_cons, struct redex_object *obj)
{
    assert(_cons);
    assert(obj);

    if (_cons->type != REDEX_CONS) {
        printf("RPLACA error: not possible on type \"%s\"\n",
                type_str(_cons->type));
        exit(-1);
    }

    _cons->cons.car = obj;
    return _cons;
}

struct redex_object *redex_number(struct redex_ctx *ctx, int value)
{
    struct redex_object *obj = redex_object_alloc(ctx);
    obj->type = REDEX_NUMBER;
    obj->number.number = value;
    return obj;
}

struct redex_object *redex_symbol(struct redex_ctx *ctx, char *value)
{
    struct redex_object *obj = redex_object_alloc(ctx);
    obj->type = REDEX_SYMBOL;
    obj->symbol.symbol = intern_string(value);
    return obj;
}

struct redex_object *redex_closure(struct redex_ctx *ctx, void *fn)
{
    struct redex_object *obj = redex_object_alloc(ctx);
    obj->type = REDEX_CLOSURE;
    obj->closure.fn = fn;
    return obj;
}

void redex_init(struct redex_ctx *ctx)
{
    struct redex_secd *secd = ctx->secd;

    redex_gc_init(ctx);

    ctx->_true  = redex_symbol(ctx, "true");
    ctx->_false = redex_symbol(ctx, "false");
    ctx->_nil   = redex_symbol(ctx, "nil");

    secd->stack   = ctx->_nil;
    secd->control = ctx->_nil;
    secd->environ = ctx->_nil;
    secd->dump    = ctx->_nil;
}
