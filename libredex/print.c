#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <redex/object.h>
#include <redex/gc.h>

void exp_print(struct redex_ctx *ctx, struct redex_object * obj)
{
    if (!obj || obj == ctx->_nil )
        return;

    if (obj->type == REDEX_NUMBER) {
        printf("%ld ", obj->number.number);
    } else if (obj->type == REDEX_SYMBOL) {
        printf("%s ", obj->symbol.symbol);
    } else if (obj->type == REDEX_CLOSURE) {
        printf("@%p ", obj->closure.fn);
    } else if (obj->type == REDEX_CONS) {
        if (redex_car(obj)->type != REDEX_CONS 
         && redex_cdr(obj)->type != REDEX_CONS 
         && !REDEX_IS_NULL(ctx, redex_cdr(obj))) {
            exp_print(ctx, redex_car(obj));
            printf(". ");
            exp_print(ctx, redex_cdr(obj));
        } else {
            if (redex_car(obj)->type == REDEX_CONS)
                printf("( ");
            exp_print(ctx, redex_car(obj));

            if (redex_car(obj)->type == REDEX_CONS)
                printf(") ");

            if (!REDEX_IS_NULL(ctx, redex_cdr(obj))) {
                exp_print(ctx, redex_cdr(obj));
            }
        }
    } else {
        printf("Unknown type: %s\n", type_str(obj->type));
        exit(-1);
    }
}
