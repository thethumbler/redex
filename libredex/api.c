#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dlfcn.h>

#include <redex/redex.h>

void redex_module_load(struct redex_ctx *ctx, const char *name)
{
    char file[50];
    snprintf(file, 50, "lib%s.so", name);

    void *handle = dlopen(file, RTLD_LAZY);

    if (handle) {
        struct redex_module *module = malloc(sizeof(struct redex_module));
        module->name = name;
        module->handle = handle;
        module->next = ctx->modules;
        ctx->modules = module;
    } else {
        fprintf(stderr, "failed to load module %s: %s\n", name, dlerror());
        exit(-1);
    }
}

static void redex_api_putc(struct redex_ctx *ctx)
{
    struct redex_secd *secd = ctx->secd;

    struct redex_object *obj = redex_car(secd->stack);
    int c = number_value(redex_car(obj));
    putchar(c);

    secd->stack = redex_cdr(secd->stack);
    return;
}

static void redex_api_puts(struct redex_ctx *ctx)
{
    struct redex_secd *secd = ctx->secd;

    struct redex_object *obj = redex_car(secd->stack);

    while (!REDEX_IS_NULL(ctx, obj) && REDEX_IS_CONS(obj)) {
        int c = number_value(redex_car(obj));
        putc(c, stderr);
        obj = redex_cdr(obj);
    }

    secd->stack = redex_cdr(secd->stack);

    return;
}

static void redex_api_print(struct redex_ctx *ctx)
{
    struct redex_secd *secd = ctx->secd;

    struct redex_object *obj = redex_car(secd->stack);
    exp_print(ctx, obj);

    secd->stack = redex_cons(ctx, ctx->_nil, redex_cdr(secd->stack));
    return;
}

static void redex_api_load(struct redex_ctx *ctx)
{
    struct redex_secd *secd = ctx->secd;

    struct redex_object *obj = redex_car(secd->stack);
    const char *module = string_value(redex_car(obj));

    redex_module_load(ctx, module);

    secd->stack = redex_cons(ctx, ctx->_nil, redex_cdr(secd->stack));
    return;
}

static void redex_api_readInt(struct redex_ctx *ctx)
{
    struct redex_secd *secd = ctx->secd;

    struct redex_object *obj = redex_car(secd->stack);

    int n;
    const char *s = string_value(redex_car(obj));

    sscanf(s, "%d", &n);

    secd->stack = redex_cons(ctx, redex_number(ctx, n), redex_cdr(secd->stack));
    return;
}

struct {
    const char *name;
    void (*fn)(struct redex_ctx *ctx);
} api_fns[] = {
    {"load", redex_api_load},
    {"putc", redex_api_putc},
    {"puts", redex_api_puts},
    {"print", redex_api_print},
    {"readInt", redex_api_readInt},
};

#define API_CNT (sizeof(api_fns)/sizeof(api_fns[0]))

struct redex_module *redex_module_find(struct redex_ctx *ctx, const char *name)
{
    struct redex_module *mod = ctx->modules;

    while (mod && strcmp(mod->name, name))
        mod = mod->next;

    return mod;
}

void redex_api_call(struct redex_ctx *ctx, const char *name)
{
    /* check if built-in function */
    for (int i = 0; i < API_CNT; ++i) {
        if (!strcmp(api_fns[i].name, name)) {
            api_fns[i].fn(ctx);
            return;
        }
    }

    char *p;
    if ((p = strchr(name, ':'))) {
        const char *mod = strndup(name, p - name);
        const char *fn  = strdup(p+1);

        struct redex_module *rmod = redex_module_find(ctx, mod);

        if (rmod) {
            char fn_name[50];
            snprintf(fn_name, 50, "rx_%s", fn);

            void (*sym)(struct redex_ctx *) = dlsym(rmod->handle, fn_name);
            if (sym) {
                sym(ctx);
                return;
            } else {
                fprintf(stderr, "could not find symbol %s in module %s\n", fn, mod);
            }
        } else {
            fprintf(stderr, "module %s is not loaded\n", mod);
            exit(-1);
        }
    }

    fprintf(stderr, "Unkown API function %s\n", name);
    exit(-1);
}
