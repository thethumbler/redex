#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include <redex/redex.h>
#include <redex/secd.h>
#include <redex/object.h>
#include <redex/gc.h>

#define NUM_CELLS 65535

void redex_gc_mark(struct redex_ctx *ctx, struct redex_object *object)
{
    if (object->marked == 0) {
        object->marked = 1;
        if (REDEX_IS_ATOM(object) == 0) {
            redex_gc_mark(ctx, redex_car(object));
            redex_gc_mark(ctx, redex_cdr(object));
        }
    }
}

void redex_gc_init(struct redex_ctx *ctx)
{
    //const unsigned cell_size = sizeof(struct redex_gc_header) + sizeof(struct redex_object);
    const unsigned cell_size = sizeof(struct redex_object);

    ctx->num_cells = getenv("REDEX_MEMORY") ?
        atoi(getenv("REDEX_MEMORY")) : NUM_CELLS;

    ctx->mem   = calloc(ctx->num_cells, cell_size);
    ctx->cells = calloc(ctx->num_cells, sizeof(struct redex_object *));

    ctx->alloc_counter = 0;
    ctx->collect_counter = 0;

    ctx->ff = NULL;

    unsigned char *ptr = ctx->mem;
    for (int i = 0; i < ctx->num_cells; i++) {
        //ctx->cells[i] = (struct redex_object *) ((struct redex_gc_header *) ptr + 1);
        ctx->cells[i] = (struct redex_object *) ptr;
        ctx->cells[i]->cons.cdr = ctx->ff;
        ctx->ff = ctx->cells[i];
        ptr += cell_size;
    }
}

void redex_gc_exit(struct redex_ctx *ctx)
{
    free(ctx->mem);
    free(ctx->cells);

    ctx->mem = NULL;
    ctx->cells = NULL;
}

struct redex_object *redex_gc_alloc(struct redex_ctx *ctx)
{
    struct redex_object *object;

    if (ctx->ff == NULL) {
        redex_gc_collect_garbage(ctx);
    }

    object = ctx->ff;
    ctx->ff = ctx->ff->cons.cdr;
    object->type = 0;

    ctx->alloc_counter++;

    return object;
}

void redex_gc_collect(struct redex_ctx *ctx)
{
    for (int i = 0; i < NUM_CELLS; i++) {
        if (ctx->cells[i]->marked == 0) {
            ctx->cells[i]->cons.cdr = ctx->ff;
            ctx->ff = ctx->cells[i];

            ctx->collect_counter++;
        }
    }
}

void redex_gc_collect_garbage(struct redex_ctx *ctx)
{
    for (int i = 0; i < NUM_CELLS; i++) {
        ctx->cells[i]->marked = 0;
    }

    redex_gc_mark(ctx, ctx->secd->stack);
    redex_gc_mark(ctx, ctx->secd->environ);
    redex_gc_mark(ctx, ctx->secd->control);
    redex_gc_mark(ctx, ctx->secd->dump);

    redex_gc_mark(ctx, ctx->work);
    redex_gc_mark(ctx, ctx->_true);
    redex_gc_mark(ctx, ctx->_false);
    redex_gc_mark(ctx, ctx->_nil);

    redex_gc_collect(ctx);

    if (ctx->ff == NULL) {
        printf("Out of memory");
        exit(-1);
    }
}

void redex_gc_stats(struct redex_ctx *ctx)
{
    printf("Cells:     %u\n", ctx->num_cells);
    printf("Allocates: %u\n", ctx->alloc_counter);
    printf("Collects:  %u\n", ctx->collect_counter);
}

