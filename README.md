Redex LispKit Implementation
============================

Building
--------
redex uses Makefiles to manage the build, simply type `$ make` to build everything

Dependencies
------------
The only dependencies required are for the build, you need any C building toolchain (we tested on gcc, though) including a compiler, a linker and a C library. One also needs `libdl` to support dynamically loaded libraries (usually shipped with the system).

The Makefiles use `gmake` (usually shipped with Linux-based distros), but the Makefiles are minimal and can easily work or be slightly modified to build with POSIX make.

Binaries
--------
The resulting files of building are
- `src/redex-secd` a SECD-machine implementation
- `compiler/compiler.secd` a redex compiler in SECD assemply
- `compiler/redexc` a binary that compiles redex programs into SECD-assembly
- `libredex/libredex.so` a shared library that provides support to all te binaries.

In order to use any binary, `libredex.so` has to be in the LD search paths in order to be picked by the runtime linker, one could acheive that by running the command `$ export LD_LIBRARY_PATH=$(pwd)/libreded` inside the source tree root, this persists to the current shell session only though.

Using
-----
A handful of examples are provided in `examples/` you could test any of them by compiling any source and running it, for example:

```
$ ./src/redex-secd -p examples/factorial.lisp > factorial.secd
$ echo "4" | ./src/redex-secd -p ./factorial.secd
```

which would give the result, 24.

C Code Generator
----------------

An optional C code generator produces C code that performs the same operations carried out by the SECD-machine and can be compiled into a native binary.

For the factorial example, the code has to be modified as follows
```lisp
(letrec main
  (factorial lambda (x) 
    (if (eq x (quote 1))
      (quote 1)
      (mul x (factorial (sub x (quote 1))))))
  (main lambda (args)
    (letrec
      (do
        (call print (factorial n))
        (call putc (quote 10)))
      (n call readInt (car (cdr args))))))
``` 
This is necessary because the generated binary calls a main function that receives a `cons` object representing the command like arguments passed to the program.

We can then compile the program as follows
```
$ ./compiler/redexc -c < factorial.lsip > factorial.c
$ gcc -I include/ -L libredex/ factorial.c runtime/rt.c -lredex -o factorial
$ ./factorial 5
```
