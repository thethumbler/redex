#include <stdio.h>
#include <string.h>

#include <redex/redex.h>

struct redex_ctx *rctx;

void closure_0();

int main(int argc, char **argv)
{
    struct redex_ctx ctx;
    struct redex_secd secd;

    memset(&ctx, 0, sizeof(ctx));
    memset(&secd, 0, sizeof(secd));

    ctx.secd = &secd;
    rctx = &ctx;

    redex_init(&ctx);

    struct redex_object *args = rctx->_nil;

    for (int i = argc - 1; i >= 0; --i) {
        args = redex_cons(rctx, redex_symbol(rctx, argv[i]), args);
    }

    args = redex_cons(&ctx, args, ctx._nil);
    secd.stack = redex_cons(&ctx, args, secd.stack);

    closure_0();

    //exp_print(&ctx, redex_car(secd.stack));
}
