(letrec ackermann
 (ackermann lambda (x y)
  (if (eq x (quote 0))
    (add y (quote 1))
    (if (eq y (quote 0))
      (ackermann (sub x (quote 1)) (quote 1))
      (ackermann (sub x (quote 1)) (ackermann x (sub y (quote 1)))))))) 
