(let null?
  (null? lambda (x)
    (if (eq x (quote nil)) 
      (quote t) 
      (quote f))))
