(letrec reverse 
  (reverse lambda (l)
    (letrec (rev0 (quote nil) l)
      (rev0 lambda (a l)
        (if (eq l (quote nil)) a
          (rev0 (cons (car l) a) (cdr l)))))))
