(letrec myprog
  (myprog lambda (x) 
    (mapcar inc x))

  (inc lambda (x) (add (quote 1) x))

  (mapcar lambda (f l)
    (if (eq l (quote nil))
      (quote nil)
      (cons (f (car l)) (mapcar f (cdr l))))))
