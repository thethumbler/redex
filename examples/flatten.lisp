(letrec flatten
  (flatten lambda (l)
    (if (eq l (quote nil)) 
      (quote nil)
      (if (atom l) 
        (cons l (quote nil))
        (if (atom (car l)) 
          (cons (car l) (flatten (cdr l)))
          (append (flatten (car l)) (flatten (cdr l)))))))
  (append lambda (x y)
    (if (eq x (quote nil)) y
      (cons (car x) (append (cdr x) y))))) 
