(letrec fibonacci
  (fibonacci lambda (x)
    (if (eq x (quote 0)) (quote 0)
    (if (eq x (quote 1)) (quote 1)
      (add (fibonacci (sub x (quote 1))) 
        (fibonacci (sub x (quote 2))))))))
