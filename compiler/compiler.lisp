(letrec compile

  ; Top leve compile
  ; input is a Cons object describing the program
  (compile lambda (syms)
    (comp syms (quote nil) (quote (AP STOP))))

  (comp lambda (syms env control)
    ; atom(x) => LD location(x, env)
    (if (atom syms)
      (cons (quote LD) (cons (location syms env) control)) 

    ;
    ; Unary Operators
    ;

    ; atom x => (comp x) ATOM
    (if (eq (car syms) (quote atom))
      (comp (car (cdr syms)) env (cons (quote ATOM) control))

    ; quote x => LDC x
    (if (eq (car syms) (quote quote)) 
      (cons (quote LDC) (cons (car (cdr syms)) control)) 

    ;
    ; Binary Operators
    ;

    ; add x y => (comp x) (comp y) ADD
    (if (eq (car syms) (quote add)) 
      (comp (car (cdr syms)) env (comp (car (cdr (cdr syms))) env (cons (quote ADD) control))) 

    ; sub x y => (comp x) (comp y) SUB
    (if (eq (car syms) (quote sub))
      (comp (car (cdr syms)) env (comp (car (cdr (cdr syms))) env (cons (quote SUB) control))) 

    ; mul x y => (comp x) (comp y) MUL
    (if (eq (car syms ) (quote mul))
      (comp (car (cdr syms)) env (comp (car (cdr (cdr syms))) env (cons (quote MUL) control))) 

    ; div x y => (comp x) (comp y) DIV
    (if (eq (car syms) (quote div))
      (comp (car (cdr syms)) env (comp (car (cdr (cdr syms))) env (cons (quote DIV) control))) 

    ; rem x y => (comp x) (comp y) REM
    (if (eq (car syms) (quote rem)) 
      (comp (car (cdr syms)) env (comp (car (cdr (cdr syms))) env (cons (quote REM) control)))

    ; leq x y => (comp x) (comp y) LEQ
    (if (eq (car syms) (quote leq))
      (comp (car (cdr syms)) env (comp (car (cdr (cdr syms))) env (cons (quote LEQ) control))) 

    ; eq x y => (comp x) (comp y) EQ
    (if (eq (car syms) (quote eq))
      (comp (car (cdr syms)) env (comp (car (cdr (cdr syms))) env (cons (quote EQ) control))) 

    ;
    ; Cons Operators
    ;

    ; car x => (comp x) CAR
    (if (eq (car syms) (quote car))
      (comp (car (cdr syms)) env (cons (quote CAR) control))

    ; cdr x => (comp x) CDR
    (if (eq (car syms) (quote cdr))
      (comp (car (cdr syms)) env (cons (quote CDR) control))

    ; cons fst snd => (comp fst) (comp snd) CONS
    (if (eq (car syms) (quote cons))
      (let
        (comp snd env (comp fst env (cons (quote CONS) control)))
        (fst car (cdr syms))
        (snd car (cdr (cdr syms))))

    ; rplaca fst snd => (comp fst) (comp snd) RPLACA
    (if (eq (car syms) (quote rplaca))
      (let
        (comp snd env (comp fst env (cons (quote RPLACA) control)))
        (fst car (cdr syms))
        (snd car (cdr (cdr syms))))


    ;
    ; if c t f => (comp c) SEL (comp t) (comp f)
    ;

    (if (eq (car syms) (quote if))
      (let
        (comp (car (cdr syms)) env (cons (quote SEL) (cons thenpt (cons elsept control))))
        (thenpt comp (car (cdr (cdr syms))) env (quote (JOIN)))
        (elsept comp (car (cdr (cdr (cdr syms)))) env (quote (JOIN))))

    ;
    ; lambda args body => LDF (comp body) RTN
    ;   with args as environ
    ;

    (if (eq (car syms) (quote lambda)) 
      (let
        (cons (quote LDF) (cons body control)) 
        (body comp (car (cdr (cdr syms))) (cons (car (cdr syms)) env) (quote (RTN))))


    ;
    ; thunk body => LDF (comp body) RTN
    ;   this is the same as lambda () body
    ;

    (if (eq (car syms) (quote thunk)) 
      (let
        (cons (quote LDF) (cons body control)) 
        (body comp (car (cdr syms)) env (quote (RTN))))

    ;
    ; do e1 e2 ... ek => (comp ek) (comp ek-1) ... (comp e1)
    ;

    (if (eq (car syms) (quote do)) 
      (letrec
        (complisr (cdr syms) env control)
        (complisr lambda (syms env control)
          (if (eq syms (quote nil))
            ; if object is nil, we are done.
            ; XXX: find a better way to return control as is
            (cons (car control) (cdr control))
            ; otherwise, complisr the cdr then comp the car of the list
            (comp (car syms) env (complisr (cdr syms) env control)))))

    ;
    ; let expr (name sub-expr)* => (comp sub-expr)* LDF ((comp exp) RTN) AP
    ;   with (names . env) as environ
    ; 
    ; compiled as an application of a lambda abstraction with the given names
    ; ((lambda names expr) args)
    ;

    (if (eq (car syms) (quote let)) 
      (let
        (let
          ; compile the list of sub-expressions after compiling the body
          (complis args env (cons (quote LDF) (cons body (cons (quote AP) control))))
          ; compile expr using the names of let-bindings as environ
          (body comp (car (cdr syms)) names (quote (RTN))))
        (names cons (vars (cdr (cdr syms))) env)
        (args exprs (cdr (cdr syms))))

    ;
    ; letrec expr (name sub-expr)* => DUM (comp sub-expr)* LDF ((comp expr) RTN) RAP
    ;   with (names . env) as environ
    ; 
    ; compiled as an application of a lambda abstraction with the given names
    ; ((lambda names expr) args)
    ;

    (if (eq (car syms) (quote letrec))
      (let
        (let
          ; same as above, except we use a trick to allow recurssion
          ; push DUM first, to create a dummy car in the env, then RAP will use replaca
          ; to replace the car with the actual list of args, creating a circular structure
          (cons (quote DUM)
            (complis args names (cons (quote LDF) (cons body (cons (quote RAP) control))))) 
          ; body is the same as in let
          (body comp (car (cdr syms)) names (quote (RTN))))
        (names cons (vars (cdr (cdr syms))) env) 
        (args exprs (cdr (cdr syms))))

    ;
    ; extension API call
    ; call e e1 e2 ... ek => (complist (e1 . e2 ... ek)) (comp e) CALL
    ;

    (if (eq (car syms) (quote call))
      (let
        (complis (cdr (cdr syms)) env (cons (quote LDC) (cons name (cons (quote CALL) control))))
        (name car (cdr syms))
      )

    ;
    ; Otherwise, we have a function application
    ; e e1 e2 ... ek => (complist (e1 . e2 ... ek)) (comp e) AP
    ;

    (complis (cdr syms) env (comp (car syms) env (cons (quote AP) control)))

    ; end all the above if statements
    ))))))))))))))))))))))


  ;
  ; compile a list
  ;

  (complis lambda (syms env control)
    (if (eq syms (quote nil))
      ; if object is nil, we are done.
      ; emit LDC . nil that will be used on the first CONS to create the list
      (cons (quote LDC) (cons (quote nil) control))
      ; otherwise, compile the car and add a CONS to control
      ; then complist the cdr of the list
      (complis (cdr syms) env (comp (car syms) env (cons (quote CONS) control)))))

  ;
  ; get the location of a sym in the environ
  ;

  (location lambda (sym env)
    (letrec
      (if (member sym (car env))
        ; if it is a member, then the first index is zero
        (cons (quote 0) (position sym (car env)))
        ; otherwise, look in the cdr, and increment the car of the result
        (incar (location sym (cdr env))))

      ; check if sym is inside env
      (member lambda (sym env)
        ; if we are at the tail, it is not a member
        (if (eq env (quote nil))
          (quote false)
        ; if it is the current head, we found it
        (if (eq sym (car env))
          (quote true)
        ; otherwise, repeat again with the cdr
        (member sym (cdr env)))))

      ; get the position (index) of sym inside env
      (position lambda (sym env) 
        (if (eq sym (car env))
          ; if it is the car, the index is zero
          (quote 0)
          ; otherwise, look in the cdr and add one to the result
          (add (quote 1) (position sym (cdr env))))) 

      (incar lambda (l)
        (cons (add (quote 1) (car l)) (cdr l)))))


  ;
  ; construct a cons object with the cars of a cons object
  ; ((1 . 2) . (3 . 4) . (5 . 6)) => (1 . 3 . 5)
  ;

  (vars lambda (d)
    (if (eq d (quote nil))
      (quote nil)
      (cons (car (car d)) (vars (cdr d)))))

  ;
  ; construct a cons object with the cdrs of a cons object
  ; ((1 . 2) . (3 . 4) . (5 . 6)) => (2 . 4 . 6)
  ;

  (exprs lambda (d)
    (if (eq d (quote nil))
      (quote nil)
      (cons (cdr (car d)) (exprs (cdr d))))) 
)
