#include <redex/runtime.h>

// closure 81
// LD ( 0 . 0 ) RTN 
static inline void closure_81()
{
    inst_ld(0, 0);
    inst_rtn();
}

// closure 80
// LDC nil LDC ( AP STOP ) CONS LDC nil CONS LD ( 0 . 0 ) CONS LD ( 1 . 1 ) AP RTN 
static inline void closure_80()
{
    inst_ldc_str("nil");
    inst_ldc_cons((struct redex_object *[]){redex_symbol(rctx, "AP"), redex_symbol(rctx, "STOP"), NULL});
    inst_cons();
    inst_ldc_str("nil");
    inst_cons();
    inst_ld(0, 0);
    inst_cons();
    inst_ld(1, 1);
    inst_ap();
    inst_rtn();
}

// closure 79
// LDC nil LDC nil LD ( 0 . 2 ) LDC AP CONS CONS LD ( 0 . 1 ) CONS LD ( 0 . 0 ) CAR CONS LD ( 1 . 1 ) AP CONS LD ( 0 . 1 ) CONS LD ( 0 . 0 ) CDR CONS LD ( 1 . 2 ) AP JOIN 
static inline void closure_79()
{
    inst_ldc_str("nil");
    inst_ldc_str("nil");
    inst_ld(0, 2);
    inst_ldc_str("AP");
    inst_cons();
    inst_cons();
    inst_ld(0, 1);
    inst_cons();
    inst_ld(0, 0);
    inst_car();
    inst_cons();
    inst_ld(1, 1);
    inst_ap();
    inst_cons();
    inst_ld(0, 1);
    inst_cons();
    inst_ld(0, 0);
    inst_cdr();
    inst_cons();
    inst_ld(1, 2);
    inst_ap();
    inst_join();
}

// closure 78
// LDC nil LD ( 1 . 2 ) LDC CALL CONS LD ( 0 . 0 ) CONS LDC LDC CONS CONS LD ( 1 . 1 ) CONS LD ( 1 . 0 ) CDR CDR CONS LD ( 2 . 2 ) AP RTN 
static inline void closure_78()
{
    inst_ldc_str("nil");
    inst_ld(1, 2);
    inst_ldc_str("CALL");
    inst_cons();
    inst_ld(0, 0);
    inst_cons();
    inst_ldc_str("LDC");
    inst_cons();
    inst_cons();
    inst_ld(1, 1);
    inst_cons();
    inst_ld(1, 0);
    inst_cdr();
    inst_cdr();
    inst_cons();
    inst_ld(2, 2);
    inst_ap();
    inst_rtn();
}

// closure 77
// LDC nil LD ( 0 . 0 ) CDR CAR CONS LDF @0x4e AP JOIN 
static inline void closure_77()
{
    inst_ldc_str("nil");
    inst_ld(0, 0);
    inst_cdr();
    inst_car();
    inst_cons();
    inst_ldf(closure_78);
    inst_ap();
    inst_join();
}

// closure 76
// LD ( 0 . 0 ) CAR LDC call EQ SEL @0x4d @0x4f JOIN 
static inline void closure_76()
{
    inst_ld(0, 0);
    inst_car();
    inst_ldc_str("call");
    inst_eq();
    inst_sel(closure_77, closure_79);
    inst_join();
}

// closure 75
// LDC nil LD ( 2 . 2 ) LDC RAP CONS LD ( 0 . 0 ) CONS LDC LDF CONS CONS LD ( 1 . 0 ) CONS LD ( 1 . 1 ) CONS LD ( 3 . 2 ) AP LDC DUM CONS RTN 
static inline void closure_75()
{
    inst_ldc_str("nil");
    inst_ld(2, 2);
    inst_ldc_str("RAP");
    inst_cons();
    inst_ld(0, 0);
    inst_cons();
    inst_ldc_str("LDF");
    inst_cons();
    inst_cons();
    inst_ld(1, 0);
    inst_cons();
    inst_ld(1, 1);
    inst_cons();
    inst_ld(3, 2);
    inst_ap();
    inst_ldc_str("DUM");
    inst_cons();
    inst_rtn();
}

// closure 74
// LDC nil LDC nil LDC ( RTN ) CONS LD ( 0 . 0 ) CONS LD ( 1 . 0 ) CDR CAR CONS LD ( 2 . 1 ) AP CONS LDF @0x4b AP RTN 
static inline void closure_74()
{
    inst_ldc_str("nil");
    inst_ldc_str("nil");
    inst_ldc_cons((struct redex_object *[]){redex_symbol(rctx, "RTN"), NULL});
    inst_cons();
    inst_ld(0, 0);
    inst_cons();
    inst_ld(1, 0);
    inst_cdr();
    inst_car();
    inst_cons();
    inst_ld(2, 1);
    inst_ap();
    inst_cons();
    inst_ldf(closure_75);
    inst_ap();
    inst_rtn();
}

// closure 73
// LDC nil LDC nil LD ( 0 . 0 ) CDR CDR CONS LD ( 1 . 5 ) AP CONS LD ( 0 . 1 ) LDC nil LD ( 0 . 0 ) CDR CDR CONS LD ( 1 . 4 ) AP CONS CONS LDF @0x4a AP JOIN 
static inline void closure_73()
{
    inst_ldc_str("nil");
    inst_ldc_str("nil");
    inst_ld(0, 0);
    inst_cdr();
    inst_cdr();
    inst_cons();
    inst_ld(1, 5);
    inst_ap();
    inst_cons();
    inst_ld(0, 1);
    inst_ldc_str("nil");
    inst_ld(0, 0);
    inst_cdr();
    inst_cdr();
    inst_cons();
    inst_ld(1, 4);
    inst_ap();
    inst_cons();
    inst_cons();
    inst_ldf(closure_74);
    inst_ap();
    inst_join();
}

// closure 72
// LD ( 0 . 0 ) CAR LDC letrec EQ SEL @0x49 @0x4c JOIN 
static inline void closure_72()
{
    inst_ld(0, 0);
    inst_car();
    inst_ldc_str("letrec");
    inst_eq();
    inst_sel(closure_73, closure_76);
    inst_join();
}

// closure 71
// LDC nil LD ( 2 . 2 ) LDC AP CONS LD ( 0 . 0 ) CONS LDC LDF CONS CONS LD ( 2 . 1 ) CONS LD ( 1 . 1 ) CONS LD ( 3 . 2 ) AP RTN 
static inline void closure_71()
{
    inst_ldc_str("nil");
    inst_ld(2, 2);
    inst_ldc_str("AP");
    inst_cons();
    inst_ld(0, 0);
    inst_cons();
    inst_ldc_str("LDF");
    inst_cons();
    inst_cons();
    inst_ld(2, 1);
    inst_cons();
    inst_ld(1, 1);
    inst_cons();
    inst_ld(3, 2);
    inst_ap();
    inst_rtn();
}

// closure 70
// LDC nil LDC nil LDC ( RTN ) CONS LD ( 0 . 0 ) CONS LD ( 1 . 0 ) CDR CAR CONS LD ( 2 . 1 ) AP CONS LDF @0x47 AP RTN 
static inline void closure_70()
{
    inst_ldc_str("nil");
    inst_ldc_str("nil");
    inst_ldc_cons((struct redex_object *[]){redex_symbol(rctx, "RTN"), NULL});
    inst_cons();
    inst_ld(0, 0);
    inst_cons();
    inst_ld(1, 0);
    inst_cdr();
    inst_car();
    inst_cons();
    inst_ld(2, 1);
    inst_ap();
    inst_cons();
    inst_ldf(closure_71);
    inst_ap();
    inst_rtn();
}

// closure 69
// LDC nil LDC nil LD ( 0 . 0 ) CDR CDR CONS LD ( 1 . 5 ) AP CONS LD ( 0 . 1 ) LDC nil LD ( 0 . 0 ) CDR CDR CONS LD ( 1 . 4 ) AP CONS CONS LDF @0x46 AP JOIN 
static inline void closure_69()
{
    inst_ldc_str("nil");
    inst_ldc_str("nil");
    inst_ld(0, 0);
    inst_cdr();
    inst_cdr();
    inst_cons();
    inst_ld(1, 5);
    inst_ap();
    inst_cons();
    inst_ld(0, 1);
    inst_ldc_str("nil");
    inst_ld(0, 0);
    inst_cdr();
    inst_cdr();
    inst_cons();
    inst_ld(1, 4);
    inst_ap();
    inst_cons();
    inst_cons();
    inst_ldf(closure_70);
    inst_ap();
    inst_join();
}

// closure 68
// LD ( 0 . 0 ) CAR LDC let EQ SEL @0x45 @0x48 JOIN 
static inline void closure_68()
{
    inst_ld(0, 0);
    inst_car();
    inst_ldc_str("let");
    inst_eq();
    inst_sel(closure_69, closure_72);
    inst_join();
}

// closure 67
// LDC nil LD ( 1 . 2 ) CONS LD ( 1 . 1 ) CONS LD ( 1 . 0 ) CDR CONS LD ( 0 . 0 ) AP RTN 
static inline void closure_67()
{
    inst_ldc_str("nil");
    inst_ld(1, 2);
    inst_cons();
    inst_ld(1, 1);
    inst_cons();
    inst_ld(1, 0);
    inst_cdr();
    inst_cons();
    inst_ld(0, 0);
    inst_ap();
    inst_rtn();
}

// closure 66
// LDC nil LDC nil LD ( 0 . 2 ) CONS LD ( 0 . 1 ) CONS LD ( 0 . 0 ) CDR CONS LD ( 1 . 0 ) AP CONS LD ( 0 . 1 ) CONS LD ( 0 . 0 ) CAR CONS LD ( 3 . 1 ) AP JOIN 
static inline void closure_66()
{
    inst_ldc_str("nil");
    inst_ldc_str("nil");
    inst_ld(0, 2);
    inst_cons();
    inst_ld(0, 1);
    inst_cons();
    inst_ld(0, 0);
    inst_cdr();
    inst_cons();
    inst_ld(1, 0);
    inst_ap();
    inst_cons();
    inst_ld(0, 1);
    inst_cons();
    inst_ld(0, 0);
    inst_car();
    inst_cons();
    inst_ld(3, 1);
    inst_ap();
    inst_join();
}

// closure 65
// LD ( 0 . 2 ) CDR LD ( 0 . 2 ) CAR CONS JOIN 
static inline void closure_65()
{
    inst_ld(0, 2);
    inst_cdr();
    inst_ld(0, 2);
    inst_car();
    inst_cons();
    inst_join();
}

// closure 64
// LD ( 0 . 0 ) LDC nil EQ SEL @0x41 @0x42 RTN 
static inline void closure_64()
{
    inst_ld(0, 0);
    inst_ldc_str("nil");
    inst_eq();
    inst_sel(closure_65, closure_66);
    inst_rtn();
}

// closure 63
// DUM LDC nil LDF @0x40 CONS LDF @0x43 RAP JOIN 
static inline void closure_63()
{
    inst_dum();
    inst_ldc_str("nil");
    inst_ldf(closure_64);
    inst_cons();
    inst_ldf(closure_67);
    inst_rap();
    inst_join();
}

// closure 62
// LD ( 0 . 0 ) CAR LDC do EQ SEL @0x3f @0x44 JOIN 
static inline void closure_62()
{
    inst_ld(0, 0);
    inst_car();
    inst_ldc_str("do");
    inst_eq();
    inst_sel(closure_63, closure_68);
    inst_join();
}

// closure 61
// LD ( 1 . 2 ) LD ( 0 . 0 ) CONS LDC LDF CONS RTN 
static inline void closure_61()
{
    inst_ld(1, 2);
    inst_ld(0, 0);
    inst_cons();
    inst_ldc_str("LDF");
    inst_cons();
    inst_rtn();
}

// closure 60
// LDC nil LDC nil LDC ( RTN ) CONS LD ( 0 . 1 ) CONS LD ( 0 . 0 ) CDR CAR CONS LD ( 1 . 1 ) AP CONS LDF @0x3d AP JOIN 
static inline void closure_60()
{
    inst_ldc_str("nil");
    inst_ldc_str("nil");
    inst_ldc_cons((struct redex_object *[]){redex_symbol(rctx, "RTN"), NULL});
    inst_cons();
    inst_ld(0, 1);
    inst_cons();
    inst_ld(0, 0);
    inst_cdr();
    inst_car();
    inst_cons();
    inst_ld(1, 1);
    inst_ap();
    inst_cons();
    inst_ldf(closure_61);
    inst_ap();
    inst_join();
}

// closure 59
// LD ( 0 . 0 ) CAR LDC thunk EQ SEL @0x3c @0x3e JOIN 
static inline void closure_59()
{
    inst_ld(0, 0);
    inst_car();
    inst_ldc_str("thunk");
    inst_eq();
    inst_sel(closure_60, closure_62);
    inst_join();
}

// closure 58
// LD ( 1 . 2 ) LD ( 0 . 0 ) CONS LDC LDF CONS RTN 
static inline void closure_58()
{
    inst_ld(1, 2);
    inst_ld(0, 0);
    inst_cons();
    inst_ldc_str("LDF");
    inst_cons();
    inst_rtn();
}

// closure 57
// LDC nil LDC nil LDC ( RTN ) CONS LD ( 0 . 1 ) LD ( 0 . 0 ) CDR CAR CONS CONS LD ( 0 . 0 ) CDR CDR CAR CONS LD ( 1 . 1 ) AP CONS LDF @0x3a AP JOIN 
static inline void closure_57()
{
    inst_ldc_str("nil");
    inst_ldc_str("nil");
    inst_ldc_cons((struct redex_object *[]){redex_symbol(rctx, "RTN"), NULL});
    inst_cons();
    inst_ld(0, 1);
    inst_ld(0, 0);
    inst_cdr();
    inst_car();
    inst_cons();
    inst_cons();
    inst_ld(0, 0);
    inst_cdr();
    inst_cdr();
    inst_car();
    inst_cons();
    inst_ld(1, 1);
    inst_ap();
    inst_cons();
    inst_ldf(closure_58);
    inst_ap();
    inst_join();
}

// closure 56
// LD ( 0 . 0 ) CAR LDC lambda EQ SEL @0x39 @0x3b JOIN 
static inline void closure_56()
{
    inst_ld(0, 0);
    inst_car();
    inst_ldc_str("lambda");
    inst_eq();
    inst_sel(closure_57, closure_59);
    inst_join();
}

// closure 55
// LDC nil LD ( 1 . 2 ) LD ( 0 . 1 ) CONS LD ( 0 . 0 ) CONS LDC SEL CONS CONS LD ( 1 . 1 ) CONS LD ( 1 . 0 ) CDR CAR CONS LD ( 2 . 1 ) AP RTN 
static inline void closure_55()
{
    inst_ldc_str("nil");
    inst_ld(1, 2);
    inst_ld(0, 1);
    inst_cons();
    inst_ld(0, 0);
    inst_cons();
    inst_ldc_str("SEL");
    inst_cons();
    inst_cons();
    inst_ld(1, 1);
    inst_cons();
    inst_ld(1, 0);
    inst_cdr();
    inst_car();
    inst_cons();
    inst_ld(2, 1);
    inst_ap();
    inst_rtn();
}

// closure 54
// LDC nil LDC nil LDC ( JOIN ) CONS LD ( 0 . 1 ) CONS LD ( 0 . 0 ) CDR CDR CDR CAR CONS LD ( 1 . 1 ) AP CONS LDC nil LDC ( JOIN ) CONS LD ( 0 . 1 ) CONS LD ( 0 . 0 ) CDR CDR CAR CONS LD ( 1 . 1 ) AP CONS LDF @0x37 AP JOIN 
static inline void closure_54()
{
    inst_ldc_str("nil");
    inst_ldc_str("nil");
    inst_ldc_cons((struct redex_object *[]){redex_symbol(rctx, "JOIN"), NULL});
    inst_cons();
    inst_ld(0, 1);
    inst_cons();
    inst_ld(0, 0);
    inst_cdr();
    inst_cdr();
    inst_cdr();
    inst_car();
    inst_cons();
    inst_ld(1, 1);
    inst_ap();
    inst_cons();
    inst_ldc_str("nil");
    inst_ldc_cons((struct redex_object *[]){redex_symbol(rctx, "JOIN"), NULL});
    inst_cons();
    inst_ld(0, 1);
    inst_cons();
    inst_ld(0, 0);
    inst_cdr();
    inst_cdr();
    inst_car();
    inst_cons();
    inst_ld(1, 1);
    inst_ap();
    inst_cons();
    inst_ldf(closure_55);
    inst_ap();
    inst_join();
}

// closure 53
// LD ( 0 . 0 ) CAR LDC if EQ SEL @0x36 @0x38 JOIN 
static inline void closure_53()
{
    inst_ld(0, 0);
    inst_car();
    inst_ldc_str("if");
    inst_eq();
    inst_sel(closure_54, closure_56);
    inst_join();
}

// closure 52
// LDC nil LDC nil LD ( 1 . 2 ) LDC RPLACA CONS CONS LD ( 1 . 1 ) CONS LD ( 0 . 0 ) CONS LD ( 2 . 1 ) AP CONS LD ( 1 . 1 ) CONS LD ( 0 . 1 ) CONS LD ( 2 . 1 ) AP RTN 
static inline void closure_52()
{
    inst_ldc_str("nil");
    inst_ldc_str("nil");
    inst_ld(1, 2);
    inst_ldc_str("RPLACA");
    inst_cons();
    inst_cons();
    inst_ld(1, 1);
    inst_cons();
    inst_ld(0, 0);
    inst_cons();
    inst_ld(2, 1);
    inst_ap();
    inst_cons();
    inst_ld(1, 1);
    inst_cons();
    inst_ld(0, 1);
    inst_cons();
    inst_ld(2, 1);
    inst_ap();
    inst_rtn();
}

// closure 51
// LDC nil LD ( 0 . 0 ) CDR CDR CAR CONS LD ( 0 . 0 ) CDR CAR CONS LDF @0x34 AP JOIN 
static inline void closure_51()
{
    inst_ldc_str("nil");
    inst_ld(0, 0);
    inst_cdr();
    inst_cdr();
    inst_car();
    inst_cons();
    inst_ld(0, 0);
    inst_cdr();
    inst_car();
    inst_cons();
    inst_ldf(closure_52);
    inst_ap();
    inst_join();
}

// closure 50
// LD ( 0 . 0 ) CAR LDC rplaca EQ SEL @0x33 @0x35 JOIN 
static inline void closure_50()
{
    inst_ld(0, 0);
    inst_car();
    inst_ldc_str("rplaca");
    inst_eq();
    inst_sel(closure_51, closure_53);
    inst_join();
}

// closure 49
// LDC nil LDC nil LD ( 1 . 2 ) LDC CONS CONS CONS LD ( 1 . 1 ) CONS LD ( 0 . 0 ) CONS LD ( 2 . 1 ) AP CONS LD ( 1 . 1 ) CONS LD ( 0 . 1 ) CONS LD ( 2 . 1 ) AP RTN 
static inline void closure_49()
{
    inst_ldc_str("nil");
    inst_ldc_str("nil");
    inst_ld(1, 2);
    inst_ldc_str("CONS");
    inst_cons();
    inst_cons();
    inst_ld(1, 1);
    inst_cons();
    inst_ld(0, 0);
    inst_cons();
    inst_ld(2, 1);
    inst_ap();
    inst_cons();
    inst_ld(1, 1);
    inst_cons();
    inst_ld(0, 1);
    inst_cons();
    inst_ld(2, 1);
    inst_ap();
    inst_rtn();
}

// closure 48
// LDC nil LD ( 0 . 0 ) CDR CDR CAR CONS LD ( 0 . 0 ) CDR CAR CONS LDF @0x31 AP JOIN 
static inline void closure_48()
{
    inst_ldc_str("nil");
    inst_ld(0, 0);
    inst_cdr();
    inst_cdr();
    inst_car();
    inst_cons();
    inst_ld(0, 0);
    inst_cdr();
    inst_car();
    inst_cons();
    inst_ldf(closure_49);
    inst_ap();
    inst_join();
}

// closure 47
// LD ( 0 . 0 ) CAR LDC cons EQ SEL @0x30 @0x32 JOIN 
static inline void closure_47()
{
    inst_ld(0, 0);
    inst_car();
    inst_ldc_str("cons");
    inst_eq();
    inst_sel(closure_48, closure_50);
    inst_join();
}

// closure 46
// LDC nil LD ( 0 . 2 ) LDC CDR CONS CONS LD ( 0 . 1 ) CONS LD ( 0 . 0 ) CDR CAR CONS LD ( 1 . 1 ) AP JOIN 
static inline void closure_46()
{
    inst_ldc_str("nil");
    inst_ld(0, 2);
    inst_ldc_str("CDR");
    inst_cons();
    inst_cons();
    inst_ld(0, 1);
    inst_cons();
    inst_ld(0, 0);
    inst_cdr();
    inst_car();
    inst_cons();
    inst_ld(1, 1);
    inst_ap();
    inst_join();
}

// closure 45
// LD ( 0 . 0 ) CAR LDC cdr EQ SEL @0x2e @0x2f JOIN 
static inline void closure_45()
{
    inst_ld(0, 0);
    inst_car();
    inst_ldc_str("cdr");
    inst_eq();
    inst_sel(closure_46, closure_47);
    inst_join();
}

// closure 44
// LDC nil LD ( 0 . 2 ) LDC CAR CONS CONS LD ( 0 . 1 ) CONS LD ( 0 . 0 ) CDR CAR CONS LD ( 1 . 1 ) AP JOIN 
static inline void closure_44()
{
    inst_ldc_str("nil");
    inst_ld(0, 2);
    inst_ldc_str("CAR");
    inst_cons();
    inst_cons();
    inst_ld(0, 1);
    inst_cons();
    inst_ld(0, 0);
    inst_cdr();
    inst_car();
    inst_cons();
    inst_ld(1, 1);
    inst_ap();
    inst_join();
}

// closure 43
// LD ( 0 . 0 ) CAR LDC car EQ SEL @0x2c @0x2d JOIN 
static inline void closure_43()
{
    inst_ld(0, 0);
    inst_car();
    inst_ldc_str("car");
    inst_eq();
    inst_sel(closure_44, closure_45);
    inst_join();
}

// closure 42
// LDC nil LDC nil LD ( 0 . 2 ) LDC EQ CONS CONS LD ( 0 . 1 ) CONS LD ( 0 . 0 ) CDR CDR CAR CONS LD ( 1 . 1 ) AP CONS LD ( 0 . 1 ) CONS LD ( 0 . 0 ) CDR CAR CONS LD ( 1 . 1 ) AP JOIN 
static inline void closure_42()
{
    inst_ldc_str("nil");
    inst_ldc_str("nil");
    inst_ld(0, 2);
    inst_ldc_str("EQ");
    inst_cons();
    inst_cons();
    inst_ld(0, 1);
    inst_cons();
    inst_ld(0, 0);
    inst_cdr();
    inst_cdr();
    inst_car();
    inst_cons();
    inst_ld(1, 1);
    inst_ap();
    inst_cons();
    inst_ld(0, 1);
    inst_cons();
    inst_ld(0, 0);
    inst_cdr();
    inst_car();
    inst_cons();
    inst_ld(1, 1);
    inst_ap();
    inst_join();
}

// closure 41
// LD ( 0 . 0 ) CAR LDC eq EQ SEL @0x2a @0x2b JOIN 
static inline void closure_41()
{
    inst_ld(0, 0);
    inst_car();
    inst_ldc_str("eq");
    inst_eq();
    inst_sel(closure_42, closure_43);
    inst_join();
}

// closure 40
// LDC nil LDC nil LD ( 0 . 2 ) LDC LEQ CONS CONS LD ( 0 . 1 ) CONS LD ( 0 . 0 ) CDR CDR CAR CONS LD ( 1 . 1 ) AP CONS LD ( 0 . 1 ) CONS LD ( 0 . 0 ) CDR CAR CONS LD ( 1 . 1 ) AP JOIN 
static inline void closure_40()
{
    inst_ldc_str("nil");
    inst_ldc_str("nil");
    inst_ld(0, 2);
    inst_ldc_str("LEQ");
    inst_cons();
    inst_cons();
    inst_ld(0, 1);
    inst_cons();
    inst_ld(0, 0);
    inst_cdr();
    inst_cdr();
    inst_car();
    inst_cons();
    inst_ld(1, 1);
    inst_ap();
    inst_cons();
    inst_ld(0, 1);
    inst_cons();
    inst_ld(0, 0);
    inst_cdr();
    inst_car();
    inst_cons();
    inst_ld(1, 1);
    inst_ap();
    inst_join();
}

// closure 39
// LD ( 0 . 0 ) CAR LDC leq EQ SEL @0x28 @0x29 JOIN 
static inline void closure_39()
{
    inst_ld(0, 0);
    inst_car();
    inst_ldc_str("leq");
    inst_eq();
    inst_sel(closure_40, closure_41);
    inst_join();
}

// closure 38
// LDC nil LDC nil LD ( 0 . 2 ) LDC REM CONS CONS LD ( 0 . 1 ) CONS LD ( 0 . 0 ) CDR CDR CAR CONS LD ( 1 . 1 ) AP CONS LD ( 0 . 1 ) CONS LD ( 0 . 0 ) CDR CAR CONS LD ( 1 . 1 ) AP JOIN 
static inline void closure_38()
{
    inst_ldc_str("nil");
    inst_ldc_str("nil");
    inst_ld(0, 2);
    inst_ldc_str("REM");
    inst_cons();
    inst_cons();
    inst_ld(0, 1);
    inst_cons();
    inst_ld(0, 0);
    inst_cdr();
    inst_cdr();
    inst_car();
    inst_cons();
    inst_ld(1, 1);
    inst_ap();
    inst_cons();
    inst_ld(0, 1);
    inst_cons();
    inst_ld(0, 0);
    inst_cdr();
    inst_car();
    inst_cons();
    inst_ld(1, 1);
    inst_ap();
    inst_join();
}

// closure 37
// LD ( 0 . 0 ) CAR LDC rem EQ SEL @0x26 @0x27 JOIN 
static inline void closure_37()
{
    inst_ld(0, 0);
    inst_car();
    inst_ldc_str("rem");
    inst_eq();
    inst_sel(closure_38, closure_39);
    inst_join();
}

// closure 36
// LDC nil LDC nil LD ( 0 . 2 ) LDC DIV CONS CONS LD ( 0 . 1 ) CONS LD ( 0 . 0 ) CDR CDR CAR CONS LD ( 1 . 1 ) AP CONS LD ( 0 . 1 ) CONS LD ( 0 . 0 ) CDR CAR CONS LD ( 1 . 1 ) AP JOIN 
static inline void closure_36()
{
    inst_ldc_str("nil");
    inst_ldc_str("nil");
    inst_ld(0, 2);
    inst_ldc_str("DIV");
    inst_cons();
    inst_cons();
    inst_ld(0, 1);
    inst_cons();
    inst_ld(0, 0);
    inst_cdr();
    inst_cdr();
    inst_car();
    inst_cons();
    inst_ld(1, 1);
    inst_ap();
    inst_cons();
    inst_ld(0, 1);
    inst_cons();
    inst_ld(0, 0);
    inst_cdr();
    inst_car();
    inst_cons();
    inst_ld(1, 1);
    inst_ap();
    inst_join();
}

// closure 35
// LD ( 0 . 0 ) CAR LDC div EQ SEL @0x24 @0x25 JOIN 
static inline void closure_35()
{
    inst_ld(0, 0);
    inst_car();
    inst_ldc_str("div");
    inst_eq();
    inst_sel(closure_36, closure_37);
    inst_join();
}

// closure 34
// LDC nil LDC nil LD ( 0 . 2 ) LDC MUL CONS CONS LD ( 0 . 1 ) CONS LD ( 0 . 0 ) CDR CDR CAR CONS LD ( 1 . 1 ) AP CONS LD ( 0 . 1 ) CONS LD ( 0 . 0 ) CDR CAR CONS LD ( 1 . 1 ) AP JOIN 
static inline void closure_34()
{
    inst_ldc_str("nil");
    inst_ldc_str("nil");
    inst_ld(0, 2);
    inst_ldc_str("MUL");
    inst_cons();
    inst_cons();
    inst_ld(0, 1);
    inst_cons();
    inst_ld(0, 0);
    inst_cdr();
    inst_cdr();
    inst_car();
    inst_cons();
    inst_ld(1, 1);
    inst_ap();
    inst_cons();
    inst_ld(0, 1);
    inst_cons();
    inst_ld(0, 0);
    inst_cdr();
    inst_car();
    inst_cons();
    inst_ld(1, 1);
    inst_ap();
    inst_join();
}

// closure 33
// LD ( 0 . 0 ) CAR LDC mul EQ SEL @0x22 @0x23 JOIN 
static inline void closure_33()
{
    inst_ld(0, 0);
    inst_car();
    inst_ldc_str("mul");
    inst_eq();
    inst_sel(closure_34, closure_35);
    inst_join();
}

// closure 32
// LDC nil LDC nil LD ( 0 . 2 ) LDC SUB CONS CONS LD ( 0 . 1 ) CONS LD ( 0 . 0 ) CDR CDR CAR CONS LD ( 1 . 1 ) AP CONS LD ( 0 . 1 ) CONS LD ( 0 . 0 ) CDR CAR CONS LD ( 1 . 1 ) AP JOIN 
static inline void closure_32()
{
    inst_ldc_str("nil");
    inst_ldc_str("nil");
    inst_ld(0, 2);
    inst_ldc_str("SUB");
    inst_cons();
    inst_cons();
    inst_ld(0, 1);
    inst_cons();
    inst_ld(0, 0);
    inst_cdr();
    inst_cdr();
    inst_car();
    inst_cons();
    inst_ld(1, 1);
    inst_ap();
    inst_cons();
    inst_ld(0, 1);
    inst_cons();
    inst_ld(0, 0);
    inst_cdr();
    inst_car();
    inst_cons();
    inst_ld(1, 1);
    inst_ap();
    inst_join();
}

// closure 31
// LD ( 0 . 0 ) CAR LDC sub EQ SEL @0x20 @0x21 JOIN 
static inline void closure_31()
{
    inst_ld(0, 0);
    inst_car();
    inst_ldc_str("sub");
    inst_eq();
    inst_sel(closure_32, closure_33);
    inst_join();
}

// closure 30
// LDC nil LDC nil LD ( 0 . 2 ) LDC ADD CONS CONS LD ( 0 . 1 ) CONS LD ( 0 . 0 ) CDR CDR CAR CONS LD ( 1 . 1 ) AP CONS LD ( 0 . 1 ) CONS LD ( 0 . 0 ) CDR CAR CONS LD ( 1 . 1 ) AP JOIN 
static inline void closure_30()
{
    inst_ldc_str("nil");
    inst_ldc_str("nil");
    inst_ld(0, 2);
    inst_ldc_str("ADD");
    inst_cons();
    inst_cons();
    inst_ld(0, 1);
    inst_cons();
    inst_ld(0, 0);
    inst_cdr();
    inst_cdr();
    inst_car();
    inst_cons();
    inst_ld(1, 1);
    inst_ap();
    inst_cons();
    inst_ld(0, 1);
    inst_cons();
    inst_ld(0, 0);
    inst_cdr();
    inst_car();
    inst_cons();
    inst_ld(1, 1);
    inst_ap();
    inst_join();
}

// closure 29
// LD ( 0 . 0 ) CAR LDC add EQ SEL @0x1e @0x1f JOIN 
static inline void closure_29()
{
    inst_ld(0, 0);
    inst_car();
    inst_ldc_str("add");
    inst_eq();
    inst_sel(closure_30, closure_31);
    inst_join();
}

// closure 28
// LD ( 0 . 2 ) LD ( 0 . 0 ) CDR CAR CONS LDC LDC CONS JOIN 
static inline void closure_28()
{
    inst_ld(0, 2);
    inst_ld(0, 0);
    inst_cdr();
    inst_car();
    inst_cons();
    inst_ldc_str("LDC");
    inst_cons();
    inst_join();
}

// closure 27
// LD ( 0 . 0 ) CAR LDC quote EQ SEL @0x1c @0x1d JOIN 
static inline void closure_27()
{
    inst_ld(0, 0);
    inst_car();
    inst_ldc_str("quote");
    inst_eq();
    inst_sel(closure_28, closure_29);
    inst_join();
}

// closure 26
// LDC nil LD ( 0 . 2 ) LDC ATOM CONS CONS LD ( 0 . 1 ) CONS LD ( 0 . 0 ) CDR CAR CONS LD ( 1 . 1 ) AP JOIN 
static inline void closure_26()
{
    inst_ldc_str("nil");
    inst_ld(0, 2);
    inst_ldc_str("ATOM");
    inst_cons();
    inst_cons();
    inst_ld(0, 1);
    inst_cons();
    inst_ld(0, 0);
    inst_cdr();
    inst_car();
    inst_cons();
    inst_ld(1, 1);
    inst_ap();
    inst_join();
}

// closure 25
// LD ( 0 . 0 ) CAR LDC atom EQ SEL @0x1a @0x1b JOIN 
static inline void closure_25()
{
    inst_ld(0, 0);
    inst_car();
    inst_ldc_str("atom");
    inst_eq();
    inst_sel(closure_26, closure_27);
    inst_join();
}

// closure 24
// LD ( 0 . 2 ) LDC nil LD ( 0 . 1 ) CONS LD ( 0 . 0 ) CONS LD ( 1 . 3 ) AP CONS LDC LD CONS JOIN 
static inline void closure_24()
{
    inst_ld(0, 2);
    inst_ldc_str("nil");
    inst_ld(0, 1);
    inst_cons();
    inst_ld(0, 0);
    inst_cons();
    inst_ld(1, 3);
    inst_ap();
    inst_cons();
    inst_ldc_str("LD");
    inst_cons();
    inst_join();
}

// closure 23
// LD ( 0 . 0 ) ATOM SEL @0x18 @0x19 RTN 
static inline void closure_23()
{
    inst_ld(0, 0);
    inst_atom();
    inst_sel(closure_24, closure_25);
    inst_rtn();
}

// closure 22
// LDC nil LDC nil LD ( 0 . 2 ) LDC CONS CONS CONS LD ( 0 . 1 ) CONS LD ( 0 . 0 ) CAR CONS LD ( 1 . 1 ) AP CONS LD ( 0 . 1 ) CONS LD ( 0 . 0 ) CDR CONS LD ( 1 . 2 ) AP JOIN 
static inline void closure_22()
{
    inst_ldc_str("nil");
    inst_ldc_str("nil");
    inst_ld(0, 2);
    inst_ldc_str("CONS");
    inst_cons();
    inst_cons();
    inst_ld(0, 1);
    inst_cons();
    inst_ld(0, 0);
    inst_car();
    inst_cons();
    inst_ld(1, 1);
    inst_ap();
    inst_cons();
    inst_ld(0, 1);
    inst_cons();
    inst_ld(0, 0);
    inst_cdr();
    inst_cons();
    inst_ld(1, 2);
    inst_ap();
    inst_join();
}

// closure 21
// LD ( 0 . 2 ) LDC nil CONS LDC LDC CONS JOIN 
static inline void closure_21()
{
    inst_ld(0, 2);
    inst_ldc_str("nil");
    inst_cons();
    inst_ldc_str("LDC");
    inst_cons();
    inst_join();
}

// closure 20
// LD ( 0 . 0 ) LDC nil EQ SEL @0x15 @0x16 RTN 
static inline void closure_20()
{
    inst_ld(0, 0);
    inst_ldc_str("nil");
    inst_eq();
    inst_sel(closure_21, closure_22);
    inst_rtn();
}

// closure 19
// LDC nil LDC nil LD ( 1 . 1 ) CDR CONS LD ( 1 . 0 ) CONS LD ( 2 . 3 ) AP CONS LD ( 0 . 2 ) AP JOIN 
static inline void closure_19()
{
    inst_ldc_str("nil");
    inst_ldc_str("nil");
    inst_ld(1, 1);
    inst_cdr();
    inst_cons();
    inst_ld(1, 0);
    inst_cons();
    inst_ld(2, 3);
    inst_ap();
    inst_cons();
    inst_ld(0, 2);
    inst_ap();
    inst_join();
}

// closure 18
// LDC nil LD ( 1 . 1 ) CAR CONS LD ( 1 . 0 ) CONS LD ( 0 . 1 ) AP LDC 0 CONS JOIN 
static inline void closure_18()
{
    inst_ldc_str("nil");
    inst_ld(1, 1);
    inst_car();
    inst_cons();
    inst_ld(1, 0);
    inst_cons();
    inst_ld(0, 1);
    inst_ap();
    inst_ldc_int(0);
    inst_cons();
    inst_join();
}

// closure 17
// LDC nil LD ( 1 . 1 ) CAR CONS LD ( 1 . 0 ) CONS LD ( 0 . 0 ) AP SEL @0x12 @0x13 RTN 
static inline void closure_17()
{
    inst_ldc_str("nil");
    inst_ld(1, 1);
    inst_car();
    inst_cons();
    inst_ld(1, 0);
    inst_cons();
    inst_ld(0, 0);
    inst_ap();
    inst_sel(closure_18, closure_19);
    inst_rtn();
}

// closure 16
// LDC nil LD ( 0 . 1 ) CDR CONS LD ( 0 . 0 ) CONS LD ( 1 . 0 ) AP JOIN 
static inline void closure_16()
{
    inst_ldc_str("nil");
    inst_ld(0, 1);
    inst_cdr();
    inst_cons();
    inst_ld(0, 0);
    inst_cons();
    inst_ld(1, 0);
    inst_ap();
    inst_join();
}

// closure 15
// LDC true JOIN 
static inline void closure_15()
{
    inst_ldc_str("true");
    inst_join();
}

// closure 14
// LD ( 0 . 0 ) LD ( 0 . 1 ) CAR EQ SEL @0xf @0x10 JOIN 
static inline void closure_14()
{
    inst_ld(0, 0);
    inst_ld(0, 1);
    inst_car();
    inst_eq();
    inst_sel(closure_15, closure_16);
    inst_join();
}

// closure 13
// LDC false JOIN 
static inline void closure_13()
{
    inst_ldc_str("false");
    inst_join();
}

// closure 12
// LD ( 0 . 1 ) LDC nil EQ SEL @0xd @0xe RTN 
static inline void closure_12()
{
    inst_ld(0, 1);
    inst_ldc_str("nil");
    inst_eq();
    inst_sel(closure_13, closure_14);
    inst_rtn();
}

// closure 11
// LDC 1 LDC nil LD ( 0 . 1 ) CDR CONS LD ( 0 . 0 ) CONS LD ( 1 . 1 ) AP ADD JOIN 
static inline void closure_11()
{
    inst_ldc_int(1);
    inst_ldc_str("nil");
    inst_ld(0, 1);
    inst_cdr();
    inst_cons();
    inst_ld(0, 0);
    inst_cons();
    inst_ld(1, 1);
    inst_ap();
    inst_add();
    inst_join();
}

// closure 10
// LDC 0 JOIN 
static inline void closure_10()
{
    inst_ldc_int(0);
    inst_join();
}

// closure 9
// LD ( 0 . 0 ) LD ( 0 . 1 ) CAR EQ SEL @0xa @0xb RTN 
static inline void closure_9()
{
    inst_ld(0, 0);
    inst_ld(0, 1);
    inst_car();
    inst_eq();
    inst_sel(closure_10, closure_11);
    inst_rtn();
}

// closure 8
// LD ( 0 . 0 ) CDR LDC 1 LD ( 0 . 0 ) CAR ADD CONS RTN 
static inline void closure_8()
{
    inst_ld(0, 0);
    inst_cdr();
    inst_ldc_int(1);
    inst_ld(0, 0);
    inst_car();
    inst_add();
    inst_cons();
    inst_rtn();
}

// closure 7
// DUM LDC nil LDF @0x8 CONS LDF @0x9 CONS LDF @0xc CONS LDF @0x11 RAP RTN 
static inline void closure_7()
{
    inst_dum();
    inst_ldc_str("nil");
    inst_ldf(closure_8);
    inst_cons();
    inst_ldf(closure_9);
    inst_cons();
    inst_ldf(closure_12);
    inst_cons();
    inst_ldf(closure_17);
    inst_rap();
    inst_rtn();
}

// closure 6
// LDC nil LD ( 0 . 0 ) CDR CONS LD ( 1 . 4 ) AP LD ( 0 . 0 ) CAR CAR CONS JOIN 
static inline void closure_6()
{
    inst_ldc_str("nil");
    inst_ld(0, 0);
    inst_cdr();
    inst_cons();
    inst_ld(1, 4);
    inst_ap();
    inst_ld(0, 0);
    inst_car();
    inst_car();
    inst_cons();
    inst_join();
}

// closure 5
// LDC nil JOIN 
static inline void closure_5()
{
    inst_ldc_str("nil");
    inst_join();
}

// closure 4
// LD ( 0 . 0 ) LDC nil EQ SEL @0x5 @0x6 RTN 
static inline void closure_4()
{
    inst_ld(0, 0);
    inst_ldc_str("nil");
    inst_eq();
    inst_sel(closure_5, closure_6);
    inst_rtn();
}

// closure 3
// LDC nil LD ( 0 . 0 ) CDR CONS LD ( 1 . 5 ) AP LD ( 0 . 0 ) CAR CDR CONS JOIN 
static inline void closure_3()
{
    inst_ldc_str("nil");
    inst_ld(0, 0);
    inst_cdr();
    inst_cons();
    inst_ld(1, 5);
    inst_ap();
    inst_ld(0, 0);
    inst_car();
    inst_cdr();
    inst_cons();
    inst_join();
}

// closure 2
// LDC nil JOIN 
static inline void closure_2()
{
    inst_ldc_str("nil");
    inst_join();
}

// closure 1
// LD ( 0 . 0 ) LDC nil EQ SEL @0x2 @0x3 RTN 
static inline void closure_1()
{
    inst_ld(0, 0);
    inst_ldc_str("nil");
    inst_eq();
    inst_sel(closure_2, closure_3);
    inst_rtn();
}

// closure 0
// DUM LDC nil LDF @0x1 CONS LDF @0x4 CONS LDF @0x7 CONS LDF @0x14 CONS LDF @0x17 CONS LDF @0x50 CONS LDF @0x51 RAP AP STOP 
void closure_0()
{
    inst_dum();
    inst_ldc_str("nil");
    inst_ldf(closure_1);
    inst_cons();
    inst_ldf(closure_4);
    inst_cons();
    inst_ldf(closure_7);
    inst_cons();
    inst_ldf(closure_20);
    inst_cons();
    inst_ldf(closure_23);
    inst_cons();
    inst_ldf(closure_80);
    inst_cons();
    inst_ldf(closure_81);
    inst_rap();
    inst_ap();
    inst_stop();
}


