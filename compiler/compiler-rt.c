#include <stdio.h>
#include <unistd.h>
#include <string.h>

#include <redex/redex.h>
#include <redex/assembler.h>

struct redex_ctx *rctx;

void closure_0();

const char *help = 
"Usage: redexc < input\n";

static int is_verbose = 0;
static int compile = 0;
static int assemble = 0;

int main(int argc, char **argv)
{
    int ch, arg;

    while ((ch = getopt(argc, argv, "vhcs")) != -1) {
        switch(ch) {
            case 'h':
                puts(help);
                return 0;
            case 'v':
                is_verbose++;
                break;
            case 'c':
                compile = 1;
                break;
            case 's':
                assemble = 1;
                break;
            case '?':
                return -1;
        }
    }

    struct redex_ctx ctx;
    struct redex_secd secd;

    memset(&ctx, 0, sizeof(ctx));
    memset(&secd, 0, sizeof(secd));

    ctx.secd = &secd;
    rctx = &ctx;

    redex_init(&ctx);

    struct redex_object *args = get_exp_list(&ctx, stdin);

    secd.stack   = redex_cons(rctx, args, rctx->_nil);
    secd.environ = rctx->_nil;
    secd.dump    = rctx->_nil;

    closure_0();

    if (assemble) {
        exp_print(&ctx, secd.stack);
        printf("\n");
    }

    if (compile)
        redex_assemble(&ctx, secd.stack);
}
