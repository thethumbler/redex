#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>

#include <redex/redex.h>
#include <redex/object.h>

#define MAX_TOKEN_LENGTH    80

/*
 * <s-exp> ::= <atom> | (<s-exp list>)
 * <s-exp list> ::= <s-exp> | <s-exp> . <s-exp> | <s-exp> <s-exp list>
 */

struct token {
    FILE *fp;
    char *file;
    char *token;
    unsigned line;
    unsigned pos;
    unsigned word;
};

static char token_space[MAX_TOKEN_LENGTH];
static struct token token;

static void start_scan(FILE *);
static void scanner();

static struct redex_object *s_exp(struct redex_ctx *ctx);
static struct redex_object *s_exp_list(struct redex_ctx *ctx);

enum {
    T_SYMBOL = 1, 
    T_NUMBER,
    T_DOT,
    T_LEFTPAREN,
    T_RIGHTPAREN,
    T_END
};

void start_scan(FILE *fp)
{
    token.fp     = fp;
    token.line = 0;
    token.pos  = 0;
    token.word = 0;
}

void scanner(void)
{
    int ch, next_ch;
    char *ptr = token_space;

    token.token = NULL;
    bzero(token_space, MAX_TOKEN_LENGTH);

    /* skip white space */
    for (; !feof(token.fp); token.pos++) {
        ch = fgetc(token.fp);

        if (!isspace(ch)) {
            ungetc(ch, token.fp);
            break;
        }

        if (ch == '\n') {
            token.line++;
            token.pos = 0;
        }
    }

    if (ch == ';') {
        fgetc(token.fp);
        while (!feof(token.fp)) {
            ch = fgetc(token.fp);
            if (ch == '\n') {
                token.line++;
                token.pos = 0;
                scanner();
                return;
            }
        }
    }

    for (; !feof(token.fp); token.pos++) {
        ch = fgetc(token.fp);
        *ptr++ = (char)ch;
        *ptr = '\0';
        if (ch == '(' || ch == ')' || ch == '.') {
            break;
        }
        next_ch = fgetc(token.fp);
        ungetc(next_ch, token.fp);
        if (isspace(next_ch) || next_ch == '(' || next_ch == ')' || next_ch == '.') {
            break;
        }
    }

    if (strlen(token_space) > 0) {
        token.token = token_space;
    }
}

const char *token_type_str(int type) {
    switch(type) {
        case T_SYMBOL: return "Symbol";
        case T_NUMBER: return "Number";
        case T_DOT: return "Dot";
        case T_LEFTPAREN: return "Left parenthesis";
        case T_RIGHTPAREN: return "Right parenthesis";
        case T_END: return "End of file";
    }
    return "Unknown token type";
}

int token_type(void)
{
    if (token.token == NULL) return T_END;
    switch (token.token[0]) {
        case '.': return T_DOT;
        case '(': return T_LEFTPAREN;
        case ')': return T_RIGHTPAREN;
        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9': return T_NUMBER;
        default:  return T_SYMBOL;
    }
}

void match(int type) {
    if (type != token_type()) {
        printf("Error - did not expect token '%s'\n", token.token);
        printf("Expected token type: %s\n", token_type_str(type));
        printf("Line %d, column %d\n", token.line, token.pos);
        exit(-1);
    }

    scanner();
}

struct redex_object *s_exp(struct redex_ctx *ctx)
{
    struct redex_object *cell = ctx->_nil;
    switch (token_type()) {
        case T_NUMBER:
            cell = redex_number(ctx, atoi(token.token));
            match(T_NUMBER);
            break;
        case T_SYMBOL:
            cell = redex_symbol(ctx, token.token);
            match(T_SYMBOL);
            break;
        case T_LEFTPAREN:
            match(T_LEFTPAREN);
            if (token_type() != T_RIGHTPAREN)
                cell = s_exp_list(ctx);
            match(T_RIGHTPAREN);
            break;
        case T_END:
            break;
        default:
            printf("Error - did not expect token '%s'\n", token.token);
            printf("Line %d, column %d\n", token.line, token.pos);
            exit(-1);
    }

    return cell;
}

struct redex_object *s_exp_list(struct redex_ctx *ctx)
{
    struct redex_object *cell = ctx->_nil;

    cell = redex_cons(ctx, s_exp(ctx), ctx->_nil);

    switch(token_type()) {
        case T_RIGHTPAREN:
            break;
        case T_DOT:
            match(T_DOT);
            cell->cons.cdr = s_exp(ctx);
            break;
        case T_END:
            break;
        default:
            cell->cons.cdr = s_exp_list(ctx);
            break;
    }

    return cell;
}

struct redex_object *get_exp(struct redex_ctx *ctx, FILE *fp)
{
    start_scan(fp);
    scanner();
    return s_exp(ctx);
}

struct redex_object *get_exp_list(struct redex_ctx *ctx, FILE *fp)
{
    start_scan(fp);
    scanner();
    return s_exp_list(ctx);
}
