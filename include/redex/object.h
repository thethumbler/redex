#ifndef _REDEX_OBJECT_H
#define _REDEX_OBJECT_H 1

#include <stdlib.h>

struct redex_number;
struct redex_symbol;
struct redex_cons;
struct redex_object;

#include <redex/redex.h>
#include <redex/secd.h>
#include <redex/gc.h>

enum {
    REDEX_NUMBER  = 1,
    REDEX_SYMBOL,
    REDEX_CONS,
    REDEX_CLOSURE,
};

struct redex_number {
    long number;
};

struct redex_symbol {
    const char *symbol;
};

struct redex_cons {
    struct redex_object *car;
    struct redex_object *cdr;
};

struct redex_closure {
    void (*fn)();
};

struct redex_object {
    int size;
    int type;
    int marked;
    int id;

    union {
        struct redex_number  number;
        struct redex_symbol  symbol;
        struct redex_cons    cons;
        struct redex_closure closure;
    };
};

struct redex_object *redex_cons(struct redex_ctx *, struct redex_object *, struct redex_object *);
struct redex_object *redex_car(struct redex_object *);
struct redex_object *redex_cdr(struct redex_object *);
struct redex_object *redex_rplaca(struct redex_object *, struct redex_object *);

struct redex_object *redex_number(struct redex_ctx *, int);
struct redex_object *redex_symbol(struct redex_ctx *, char *);
struct redex_object *redex_closure(struct redex_ctx *, void *);

const char *intern_string(char *string);
void intern_free(void);

void exp_print(struct redex_ctx *, struct redex_object *);
struct redex_object *get_exp(struct redex_ctx *, FILE *);
struct redex_object *get_exp_list(struct redex_ctx *, FILE *);

#define REDEX_IS_SYMBOL(obj) ((obj)->type == REDEX_SYMBOL)
#define REDEX_IS_NUMBER(obj) ((obj)->type == REDEX_NUMBER)
#define REDEX_IS_ATOM(obj)   (REDEX_IS_SYMBOL((obj)) || REDEX_IS_NUMBER(obj))
#define REDEX_IS_CONS(obj)   ((obj)->type == REDEX_CONS)

#define REDEX_IS_NULL(ctx, obj) \
    (((obj)->type == REDEX_SYMBOL) && (!strcmp(string_value((obj)), string_value((ctx)->_nil))))

static inline const char *string_value(struct redex_object *obj)
{
    if (obj->type != REDEX_SYMBOL) {
        fprintf(stderr, "Object is not a symbol\n");
        exit(-1);
    }

    return obj->symbol.symbol;
}

static inline int number_value(struct redex_object *obj)
{
    if (obj->type != REDEX_NUMBER) {
        printf("Object is not a number\n");
        exit(-1);
    }

    return obj->number.number;
}

static inline const char *type_str(int type)
{
    switch (type) {
        case REDEX_CONS:   return "Cons";
        case REDEX_NUMBER: return "Number";
        case REDEX_SYMBOL: return "Symbol";
        case REDEX_CLOSURE: return "Closure";
    }

    return "Unknown";
}

#endif /* ! _REDEX_OBJECT_H */
