#ifndef _REDEX_ASSEMBLER_H
#define _REDEX_ASSEMBLER_H

#include <redex/redex.h>

void redex_assemble(struct redex_ctx *, struct redex_object *);

#endif /* ! _REDEX_ASSEMBLER_H */
