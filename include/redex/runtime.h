#ifndef _REDEX_RUNTIME_H
#define _REDEX_RUNTIME_H 1

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <redex/redex.h>

//#define DEBUG(...)  fprintf(stderr, __VA_ARGS__)
#define DEBUG(...)  

extern struct redex_ctx *rctx;

static inline struct redex_object *locate(unsigned i, unsigned j, struct redex_object *env)
{
    DEBUG("locate(%d, %d, %p)\n", i, j, env);

    struct redex_object *obj = env;

    while (i--)
        obj = redex_cdr(obj);

    obj = redex_car(obj);

    while (j--)
        obj = redex_cdr(obj);

    obj = redex_car(obj);

    return obj;
}

static inline void inst_ld(int i, int j)
{
    DEBUG("inst_ld(%d, %d)\n", i, j);

    struct redex_secd *secd = rctx->secd;
    struct redex_object *obj = locate(i, j, secd->environ);
    rctx->work = obj;
    secd->stack = redex_cons(rctx, obj, secd->stack);
}

static inline void inst_ldc_int(int x)
{
    DEBUG("inst_ldc_int(%d)\n", x);

    struct redex_secd *secd = rctx->secd;
    secd->stack = redex_cons(rctx, redex_number(rctx, x), secd->stack);
}

static inline void inst_ldc_str(char *sym)
{
    DEBUG("inst_ldc_str(%s)\n", sym);

    struct redex_secd *secd = rctx->secd;
    secd->stack = redex_cons(rctx, redex_symbol(rctx, sym), secd->stack);
}

static inline void inst_ldc_cons(struct redex_object *objs[])
{
    DEBUG("inst_ldc_cons(%p)\n", objs);

    struct redex_object **obj = objs;

    struct redex_object *cons = rctx->_nil;

    /* seek to last element */
    while (*obj) ++obj;
    --obj;

    while (obj >= objs) {
        cons = redex_cons(rctx, *obj, cons);
        --obj;
    }

    struct redex_secd *secd = rctx->secd;
    secd->stack = redex_cons(rctx, cons, secd->stack);
}

static inline void inst_ldf(void *fn)
{
    DEBUG("inst_ldf(%p)\n", fn);

    struct redex_secd *secd = rctx->secd;
    struct redex_object *f = redex_closure(rctx, fn);

    secd->stack = redex_cons(rctx, redex_cons(rctx, f, secd->environ), secd->stack);
}

static inline void inst_ap(void)
{
    DEBUG("inst_ap()\n");

    struct redex_secd *secd = rctx->secd;

    struct redex_object *s = redex_cdr(redex_cdr(secd->stack));
    struct redex_object *e = secd->environ;
    struct redex_object *d = secd->dump;

    struct redex_object *f = redex_car(redex_car(secd->stack));
    struct redex_object *v = redex_car(redex_cdr(secd->stack));
    struct redex_object *_e = redex_cdr(redex_car(secd->stack));

    secd->dump    = redex_cons(rctx, s, redex_cons(rctx, e, d));
    secd->environ = redex_cons(rctx, v, _e);
    secd->stack   = rctx->_nil;

    if (f->type != REDEX_CLOSURE) {
        fprintf(stderr, "error: f is not a closure\n");
        abort();
    }

    f->closure.fn();
}

static inline void inst_sel(void (*ct)(void), void (*cf)(void))
{
    DEBUG("inst_sel(%p, %p)\n", ct, cf);

    struct redex_secd *secd = rctx->secd;

    if (!strcmp(string_value(redex_car(secd->stack)), string_value(rctx->_true))) {
        secd->stack   = redex_cdr(secd->stack);
        ct();
    } else {
        secd->stack   = redex_cdr(secd->stack);
        cf();
    }
}

#define inst_rtn() \
    DEBUG("inst_rtn()\n"); \
    struct redex_secd *secd = rctx->secd; \
    struct redex_object *r = redex_car(secd->stack); \
    struct redex_object *s = redex_car(secd->dump); \
    secd->stack   = redex_cons(rctx, r, s); \
    secd->environ = redex_car(redex_cdr(secd->dump)); \
    secd->dump    = redex_cdr(redex_cdr(secd->dump)); \
    return;

#define inst_join() \
    DEBUG("inst_join()\n"); \
    return;

static inline void inst_stop(void)
{
    DEBUG("inst_stop()\n");
    //exit(0);
    return;
}

static inline void inst_eq(void)
{
    DEBUG("inst_eq()\n");

    struct redex_secd *secd = rctx->secd;

    struct redex_object *a = redex_car(secd->stack);
    struct redex_object *b = redex_car(redex_cdr(secd->stack));

    int r = 0;

    if (REDEX_IS_SYMBOL(a) && REDEX_IS_SYMBOL(b)) {
        r = !strcmp(string_value(a), string_value(b));
    } else if (REDEX_IS_NUMBER(a) && REDEX_IS_NUMBER(b)) {
        r = number_value(a) == number_value(b);
    }

    if (r) {
        secd->stack = redex_cons(rctx, rctx->_true, redex_cdr(redex_cdr(secd->stack)));
    } else {
        secd->stack = redex_cons(rctx, rctx->_false, redex_cdr(redex_cdr(secd->stack)));
    }
}

static inline void inst_add(void)
{
    DEBUG("inst_add()\n");

    struct redex_secd *secd = rctx->secd;

    struct redex_object *a = redex_car(secd->stack);
    struct redex_object *b = redex_car(redex_cdr(secd->stack));

    int r = number_value(b) + number_value(a);

    secd->stack = redex_cons(rctx, redex_number(rctx, r), redex_cdr(redex_cdr(secd->stack)));
}

static inline void inst_sub(void)
{
    DEBUG("inst_sub()\n");

    struct redex_secd *secd = rctx->secd;

    struct redex_object *a = redex_car(secd->stack);
    struct redex_object *b = redex_car(redex_cdr(secd->stack));

    int r = number_value(b) - number_value(a);

    secd->stack = redex_cons(rctx, redex_number(rctx, r), redex_cdr(redex_cdr(secd->stack)));
}

static inline void inst_mul(void)
{
    DEBUG("inst_mul()\n");

    struct redex_secd *secd = rctx->secd;

    struct redex_object *a = redex_car(secd->stack);
    struct redex_object *b = redex_car(redex_cdr(secd->stack));

    int r = number_value(b) * number_value(a);

    secd->stack = redex_cons(rctx, redex_number(rctx, r), redex_cdr(redex_cdr(secd->stack)));
}

static inline void inst_div(void)
{
    DEBUG("inst_div()\n");

    struct redex_secd *secd = rctx->secd;

    struct redex_object *a = redex_car(secd->stack);
    struct redex_object *b = redex_car(redex_cdr(secd->stack));

    int r = number_value(b) / number_value(a);

    secd->stack = redex_cons(rctx, redex_number(rctx, r), redex_cdr(redex_cdr(secd->stack)));
}

static inline void inst_rem(void)
{
    DEBUG("inst_rem()\n");

    struct redex_secd *secd = rctx->secd;

    struct redex_object *a = redex_car(secd->stack);
    struct redex_object *b = redex_car(redex_cdr(secd->stack));

    int r = number_value(b) % number_value(a);

    secd->stack = redex_cons(rctx, redex_number(rctx, r), redex_cdr(redex_cdr(secd->stack)));
}

static inline void inst_leq(void)
{
    DEBUG("inst_leq()\n");

    struct redex_secd *secd = rctx->secd;

    struct redex_object *a = redex_car(secd->stack);
    struct redex_object *b = redex_car(redex_cdr(secd->stack));

    int r = number_value(b) <= number_value(a);

    if (r) {
        secd->stack = redex_cons(rctx, rctx->_true, redex_cdr(redex_cdr(secd->stack)));
    } else {
        secd->stack = redex_cons(rctx, rctx->_false, redex_cdr(redex_cdr(secd->stack)));
    }
}

static inline void inst_dum(void)
{
    DEBUG("inst_dum()\n");

    struct redex_secd *secd = rctx->secd;
    secd->environ = redex_cons(rctx, rctx->_nil, secd->environ);
}

static inline void inst_car(void)
{
    DEBUG("inst_car()\n");

    struct redex_secd *secd = rctx->secd;

    struct redex_object *obj = redex_car(secd->stack);
    secd->stack = redex_cons(rctx, redex_car(obj), redex_cdr(secd->stack));
}

static inline void inst_cdr(void)
{
    DEBUG("inst_cdr()\n");

    struct redex_secd *secd = rctx->secd;

    struct redex_object *obj = redex_car(secd->stack);
    secd->stack = redex_cons(rctx, redex_cdr(obj), redex_cdr(secd->stack));
}

static inline void inst_cons(void)
{
    DEBUG("inst_cons()\n");

    struct redex_secd *secd = rctx->secd;
    struct redex_object *a = redex_car(secd->stack);
    struct redex_object *b = redex_car(redex_cdr(secd->stack));
    struct redex_object *obj = redex_cons(rctx, a,b);

    secd->stack = redex_cons(rctx, obj, redex_cdr(redex_cdr(secd->stack)));
}

static inline void inst_atom(void)
{
    DEBUG("inst_atom()\n");

    struct redex_secd *secd = rctx->secd;

    struct redex_object *obj = redex_car(secd->stack);

    if (REDEX_IS_ATOM(obj)) {
        secd->stack = redex_cons(rctx, rctx->_true, redex_cdr(secd->stack));
    } else {
        secd->stack = redex_cons(rctx, rctx->_false, redex_cdr(secd->stack));
    }
}

static inline void inst_rap(void)
{
    DEBUG("inst_rap()\n");

    struct redex_secd *secd = rctx->secd;

    struct redex_object *S = redex_cdr(redex_cdr(secd->stack));
    struct redex_object *E = redex_cdr(secd->environ);
    struct redex_object *D = secd->dump;

    struct redex_object *f = redex_car(redex_car(secd->stack));
    struct redex_object *e = redex_cdr(redex_car(secd->stack));
    struct redex_object *v = redex_car(redex_cdr(secd->stack));

    secd->dump = redex_cons(rctx, S, redex_cons(rctx, E, D));
    secd->environ = e;
    secd->control = f;
    secd->stack = rctx->_nil;

    redex_rplaca(e, v);

    if (f->type != REDEX_CLOSURE) {
        fprintf(stderr, "error: f is not a closure\n");
        abort();
    }

    f->closure.fn();
}

static inline void inst_rplaca(void)
{
    struct redex_secd *secd = rctx->secd;

    struct redex_object *_cons = redex_car(secd->stack);
    struct redex_object *obj   = redex_car(redex_cdr(secd->stack));

    redex_rplaca(_cons, obj);

    secd->stack = redex_cons(rctx, _cons, redex_cdr(redex_cdr(secd->stack)));
}

static inline void inst_call(void)
{
    struct redex_secd *secd = rctx->secd;
    struct redex_object *obj = redex_car(secd->stack);
    secd->stack = redex_cdr(secd->stack);
    const char *s = string_value(obj);
    redex_api_call(rctx, s);
}

#endif /* ! _REDEX_RUNTIME_H */
