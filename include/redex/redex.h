#ifndef _REDEX_REDEX_H
#define _REDEX_REDEX_H 1

struct redex_ctx;
struct redex_module;

#include <redex/object.h>
#include <redex/secd.h>

struct redex_module {
    const char *name;
    void *handle;

    struct redex_module *next;
};

struct redex_ctx {
    struct redex_secd *secd;
    struct redex_object *work;

    unsigned alloc_counter;
    unsigned collect_counter;
    unsigned num_cells;

    void *mem;
    struct redex_object **cells;

    struct redex_object *ff;
    struct redex_object *_true;
    struct redex_object *_false;
    struct redex_object *_nil;

    struct redex_module *modules;
};

void redex_api_call(struct redex_ctx *ctx, const char *name);
void redex_init(struct redex_ctx *ctx);
struct redex_object *redex_execute(struct redex_ctx *, struct redex_object *fn, struct redex_object *args);

#endif /* ! _REDEX_REDEX_H */
