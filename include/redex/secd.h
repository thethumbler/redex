#ifndef _REDEX_SECD_H
#define _REDEX_SECD_H 1

struct redex_secd;

#include <redex/object.h>

struct redex_secd {
    struct redex_object *stack;
    struct redex_object *environ;
    struct redex_object *control;
    struct redex_object *dump;
    int stopped;
};

#endif /* ! _REDEX_SECD_H */
