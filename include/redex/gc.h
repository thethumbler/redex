#ifndef _REDEX_GC_H
#define _REDEX_GC_H 1

#include <redex/redex.h>

void redex_gc_init(struct redex_ctx *);
struct redex_object *redex_gc_alloc(struct redex_ctx *);
void redex_gc_collect_garbage(struct redex_ctx *);
void redex_gc_collect(struct redex_ctx *);
void redex_gc_mark(struct redex_ctx *, struct redex_object *);
void redex_gc_exit(struct redex_ctx *);
void redex_gc_stats(struct redex_ctx *);

#endif
